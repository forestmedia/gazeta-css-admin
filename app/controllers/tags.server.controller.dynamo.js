'use strict';

/**
 * Module dependencies.
 */

var AWS = require('aws-sdk');
var	errorHandler = require('./errors.server.controller'),
 	_ = require('lodash');

  AWS.config.update({
    region: "us-east-1"
  });

var docClient = new AWS.DynamoDB.DocumentClient(); //Document Client AWS SDK to read and write data to Amazon DynamoDB


/**
 * Show the current Tag
 */
  exports.read = function(req, res) {
  	res.jsonp(req.tag);
  };

/**
 * Tag middleware
 */
  exports.tagByID = function(req, res, next, id) {

    id = id.toLowerCase();
     var last =  undefined;
     var count = req.query.count || 6;
     var articles = [];

     if(req.query.start){
       last = JSON.parse(req.query.start);
     }

     var params = {

   	    TableName: 'Hashtag',
   	    IndexName: 'date-index',
   			KeyConditionExpression: 'label = :value',
        FilterExpression: '#status > :val',
   			Limit: count,
   	    ExpressionAttributeNames: {
   	        '#status': 'status',
            '#id' : '_id'
   	    },
   	    ExpressionAttributeValues: {
   	      ':value': id,
          ':val' : 1
   	    },
   			ProjectionExpression: '#id',
   			ExclusiveStartKey:last,
   	    ScanIndexForward: false,
   	    ReturnConsumedCapacity: 'NONE'
       };

  		docClient.query(params, doQuery);

        function doQuery(err, data){
        if(!data || err){

            res.status = 404;
            res.json({message:'No results'});

        }else{

          // Print the results

          for (var i = 0; i < data.Items.length; i++ ) {
              articles.push(data.Items[i]);
          }

          // More data.  Keep calling scan.

          if (articles.length < count && data.LastEvaluatedKey) {
              params.ExclusiveStartKey = data.LastEvaluatedKey;
              docClient.query(params, doQuery);
          } else {

            var par = {

            RequestItems: {

                Article: {
                    Keys: articles
                }
             }
            };


            docClient.batchGet(par, (error, resp) => {
              console.log(articles);
                  if(!resp||error){

                      res.status = 404;
                      res.json({message:'No results'});

                  }else{

                        var respuesta = {
                          results:resp.Responses.Article,
                          lastKey:data.LastEvaluatedKey
                        };

                          res.json(respuesta);
                  }
                });
             }

          }

  		};// Close Function doQuery
  };//Close Query

  /**
   * Tag middleware (server)
   */
  exports.tagByServer = function(req, res, next, id) {

     var last =  undefined;
     var count = req.query.count || 6;
     var articles = [];

     if(req.query.start){
       last = JSON.parse(req.query.start);
     }

       var id = req.query.filter.tag;

     var params = {

   	    TableName: 'Hashtag',
   	    IndexName: 'date-index',
   			KeyConditionExpression: 'label = :value',
        FilterExpression: '#status > :val',
   			Limit: count,
   	    ExpressionAttributeNames: {
   	        '#status': 'status',
            '#id' : '_id'
   	    },
   	    ExpressionAttributeValues: {
   	      ':value': id,
          ':val' : 1
   	    },
   			ProjectionExpression: '#id',
   			ExclusiveStartKey:last,
   	    ScanIndexForward: false,
   	    ReturnConsumedCapacity: 'NONE'
       };

  		docClient.query(params, doQuery);

        function doQuery(err, data){
        if(!data || err){

            res({message:'No results'});

        }else{

          // Print the results
          for (var i = 0; i < data.Items.length; i++ ) {
              articles.push(data.Items[i]);
          }

          // More data.  Keep calling scan.
          if (articles.length < count && data.LastEvaluatedKey) {
              params.ExclusiveStartKey = data.LastEvaluatedKey;
              docClient.query(params, doQuery);
          } else {
            var par = {

            RequestItems: {

                Article: {
                    Keys: articles
                }
             }
            };

            docClient.batchGet(par, (error, resp) => {

                  if(!resp||error){

                  res(error);

                  }else{
                        var respuesta = {
                          results:resp.Responses.Article,
                          lastKey:data.LastEvaluatedKey
                        };
                          res(respuesta);
                  }
                });
             }

          }

  		};// Close Function doQuery
  };//Close Query

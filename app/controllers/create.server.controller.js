'use strict';
/**
 * Module dependencies.
 */
 var	errorHandler = require('./errors.server.controller'),
 	_ = require('lodash');
	var	articles = require('../../app/controllers/articles.server.controller.dynamo');
  var	tags = require('../../app/controllers/tags.server.controller.dynamo');
	var banners = require('../../app/controllers/banners.server.controller.dynamo');
  var poll = require('../../app/controllers/poll.server.controller.dynamo');
  var core = require('../../app/controllers/core.server.controller');
	const fs = require('fs');
  const async = require('async');
  var io = require('socket.io')(8022);
  var AWS = require('aws-sdk');
  var s3 = new AWS.S3();
  var config = {};


  io.on('connection', function (socket) {
    s3.getObject({
      Bucket: 'gazeta-ccs-files',
      Key: 'model.json'
    }, (err, resp) => {
      var arch = JSON.parse(resp.Body);
      io.emit('notification', arch);
    });
  });

  exports.settingsfile = function(req, res) {
      var file = JSON.stringify(req.body);
      s3.putObject({
        Bucket: 'gazeta-ccs-files',
        Key: 'config.json',
        Body: file
      }, resp => {
        res.json('Config Save');
        config = req.body;
        creator();
      });

  };

var preParams = [
 {
	 query:
	 {
	 	server:true , count:20
  },
  func:function(obj, callback){
      articles.listServer(obj, (data, err)=> {
        if(err) callback(err);
        else  modelo['articles'] = data; callback();
      });
  }

 },
 {
	 query:
	 {
	 	server:true , count:30,
		filter:{
			active:true
		}
  },
  func:function(obj, callback){
      banners.search(obj, (data, err)=> {
        if(err) callback(err);
        else 	modelo['banners'] = data; callback();
      });
  }

 },

 {
	 query:
	 {
	 	server:true , count:12,
		filter:{
			tag:'política'
		}
  },
  func:function(obj, callback){
      tags.tagByServer(obj, (data, err)=> {
        if(err) callback(err);
        else  modelo['Política'] = data; callback();
      });
  }
 },
 {
	 query:
	 {
	 	server:true , count:12,
		filter:{
			tag:'economía'
		}
  },
  func:function(obj, callback){
      tags.tagByServer(obj, (data, err)=> {
        if(err) callback(err);
        else  modelo['Economía'] = data; callback();
      });
  }
 },
 {
	 query:
	 {
	 	server:true , count:12,
		filter:{
			tag:'sucesos'
		}
  },
  func:function(obj, callback){
      tags.tagByServer(obj, (data, err)=> {
        if(err) callback(err);
        else  modelo['Sucesos'] = data; callback();
      });
  }

 },
{
	 query:
	 {
	 	server:true , count:12,
		filter:{
			tag:'opinión'
		}
  },
  func:function(obj, callback){
      tags.tagByServer(obj, (data, err)=> {
        if(err) callback(err);
        else  modelo['Opinión'] = data; callback();
      });
  }
 },
 {
	 query:
	 {
	 	server:true , count:1,
		filter:{
			tag:'imagen'
		},
  },
  func:function(obj, callback){
      tags.tagByServer(obj, (data, err)=> {
        if(err) callback(err);
        else  modelo['Imagen'] = data; callback();
      });
  }
 }
 ];


var modelo = {
	articles : {},
	banners : {},
	Política: {},
	Economía:{},
	Sucesos:{},
	Opinión:{},
	Imagen:{},
	update_date:""
};

function juntar (n){
  return _.map(n.widget, clean);
}

function clean(l){
  if(l.arrayElement.length == 0 || l.arrayElement.length == undefined){
    delete l.arrayElement;
  }
  return l
}


s3.getObject({
    Bucket: 'gazeta-ccs-files',
    Key: 'config.json'
  }, (err, res)=>{
    config = JSON.parse(res.Body);
    console.log(config);
     creator();
  });


var busy = false;
function creator(){

  var petArr = [];
  modelo['widgets'] = {
    tv :[],
    poll:[],
    custom:[],
    likes:[],
    popular:[],
    hashtag:[],
    user:[]
  };



  //petArr = _.extend(_.map(_.uniqBy(_.flatten(_.map(config, juntar)), 'input'), execPetra), preParams);
petArr = _.union(_.compact(_.map(_.differenceBy(_.flatten(_.map(config, juntar)), 'input'), execPetra)),preParams);

  var funcArr = [];
    petArr.forEach((w)=>{
      funcArr.push(function(callback){
          w.func(w, (data , err) => {
              if(err){ callback(err);}else{
                callback(null, data);
              }
          });
      });
    });

  function addId (obj){

    return {_id : obj}

  }

  async.parallel(funcArr , (err, results) => {
      if(err) {console.error(err);}
      else {
        modelo['update_date'] = new Date;
        var fecha = modelo.update_date;
        var archivo = JSON.stringify(modelo);

      s3.putObject({
        Bucket: 'gazeta-ccs-files',
        Key: 'model.json',
        Body: archivo
      }, resp => {
        console.log('Successfully uploaded package.');
      });


      }
  });

  function execPetra(widget){

    if (widget.type == 'tv') {
      var obj = {
        type: widget.type,
        title:widget.input,
        query:{
          count:widget.count,
          _id:widget.input
        },
        func:function(obj, callback){
          core.hvideos(obj, (data, err)=> {
            if(err) callback(err);
            else  modelo.widgets.tv.push({title:widget.input, results:data}); callback();
          });
        }
      };
      return obj;

    }

    else if (widget.type == 'poll') {
      var obj = {
        type: widget.type,
        query:{
          _id:widget.input
        },
        func:function(obj , callback){
          poll.pollByServer(obj, (data, err)=> {
            if(err) callback(err);
            else  modelo.widgets.poll.push({title:widget.input, results:data}); callback();
          });
        }
      };
      return obj;
    }

    else if (widget.type == 'custom') {

      var obj = {
        type: widget.type,
        title:widget.input,
        query:{
          arrayElement:_.map(widget.arrayElement, addId)
        },
        func:function(obj, callback){
            articles.listCustom(obj, (data, err)=> {
              if(err) {
                callback(err);
              }
              else {
                modelo.widgets.custom.push({title:widget.input, results:data});
                callback();
              }
             });
        }
      };
      return obj;
    }

    else if (widget.type == 'hashtag') {

      var obj = {
        type: widget.type,
        title:widget.input,
        query:{
          server:true,
          count:widget.count,
          filter:{
            tag:widget.input
          }
        },
        func:function(obj, callback){
          	tags.tagByServer(obj, (data, err)=> {
              if(err) {callback(err);}
              else {
                modelo.widgets.hashtag.push({title:widget.input, results:data});
                callback();
                }
            });
        }
      };
      return obj;
    }

    else if (widget.type == 'user') {

      var obj = {
        type: widget.type,
        title:widget.input,
        query:{
          server:true,
          count:widget.count,
          filter:{
            tag:widget.input
          }
        },
        func:function(obj, callback){
            tags.tagByServer(obj, (data, err)=> {
              if(err) {
                callback(err);
              }
              else {
                modelo.widgets.user.push({title:widget.input, results:data});
                callback();
              }
             });
        }
      };
      return obj;
    }


    else if (widget.type == 'popular') {
      return
    }

    else if (widget.type == 'likes') {
      var obj = {
        type: widget.type,
        query:{
          count:widget.count
        },
        func:function(obj, callback){
            articles.listLikes(obj, (data, err)=> {
              if(err) {
                callback(err);
              }
              else {
                modelo.widgets.likes.push(data);
                callback();
              }
             });
        }
      };

      return obj;

    } else {
      return;
    }
  }


}


exports.model = function(req, res){
//  var cosa = memcached.get('modelo');
  res.header('Cache-Control', 'public, max-age=60000');
  res.send(cosa);
}

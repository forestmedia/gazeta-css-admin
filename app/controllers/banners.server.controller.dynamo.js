'use strict';

/**
* Module dependencies.
*/

var AWS = require('aws-sdk');
var errorHandler = require('./errors.server.controller'),
	_ = require('lodash');

	AWS.config.update({
		region: "us-east-1"
	});

var docClient = new AWS.DynamoDB.DocumentClient(); //Document Client AWS SDK to read and write data to Amazon DynamoDB
var dynamo = new AWS.DynamoDB(); //DynamoDB object
var async = require('async');
var generate = require('node-uuid');

/**
* Create a Banner.
*/
	exports.create = function(req, res) {

		req.body.client.banners = [];
		req.body.client._id = generate.v4();
		var Banners =[];

			for(var i=0;i<req.body.banners.length;i++){

				req.body.banners[i].status = Number(req.body.banners[i].status);
				req.body.banners[i].site = 'GazetaCCs';
				req.body.banners[i]._id = generate.v4();
				req.body.banners[i].plan = req.body.client.plan;
				var	obj= { // PutRequest

						PutRequest: {

								Item:req.body.banners[i]
						}
				};

					Banners.push(obj);
					console.log(obj);
					req.body.client.banners.push({_id:req.body.banners[i]._id});
			};

			var params = {

				RequestItems: {

						Banner: Banners
		    	}
	    	};


		docClient.batchWrite(params, (err, banners) => {
			console.log(err);
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {

				var par = {
						TableName: "Client",
								// Insert request por body to item params
								Item: req.body.client
						};

					docClient.put(par, (err, cliente) => {

							if (err) {
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
							} else {
								res.json({_id:req.body.client._id});
							}
					});
				}
		});
	};

/**
 * Show the current Banner
 */
	exports.read = function(req, res) {
		res.jsonp(req.banner);
	};

/**
 * Update a Banner
 */
 exports.update = function(req, res) {

 	req.body.client.banners = [];
 	var Banners =[];

	 	for(var i=0;i<req.body.banners.length;i++){

	 		req.body.banners[i].status = Number(req.body.banners[i].status);
	 		req.body.banners[i].site = 'GazetaCCs';
			req.body.banners[i].plan = req.body.client.plan;

				if(!req.body.banners[i]._id){
					req.body.banners[i]._id = generate.v4();
				}

		 		var	obj= { //PutRequest

						PutRequest: {
								Item:req.body.banners[i]
						}
		 		};

		 		Banners.push(obj);
		 		req.body.client.banners.push({_id:req.body.banners[i]._id});
	 	};

		for(var i=0;i<req.body.removed.length;i++){

			var	obj= { //PutRequest

					DeleteRequest: {

							Key:{_id:req.body.removed[i]._id}
					}
			};

							Banners.push(obj);
		};

	 		var params = {

		 			RequestItems: { // A map of TableName to Put or Delete requests for that table
		 					Banner: Banners
		 	    }
	     };

		 	docClient.batchWrite(params, (err, banners) => {

				 		if (err) {

				 			return res.status(400).send({
				 				message: errorHandler.getErrorMessage(err)
				 			});
				 		} else {

				 			var par = {

			 					TableName: "Client",
			 							// Insert request por body to item params
			 							Item: req.body.client
				 			};

				 			docClient.put(par, (err, cliente) => {
					 				if (err) {
					 					return res.status(400).send({
					 						message: errorHandler.getErrorMessage(err)
					 					});
					 				} else {
					 					res.json({_id:req.body.client._id});
					 				}
				 			});

				 		}
		 	});
 };

/**
 * Delete Banner
 */
	exports.delete = function(req, res) {

		var Banners = [];

			async.waterfall([
				//Delete Banner if exist
				function(done){

					if(req.banner.banners.length > 0){
							req.banner.banners.forEach(function(element){
									var	obj= { // PutRequest

												DeleteRequest: {

														Key:{_id:element._id}
												}
									};

										Banners.push(obj);
							});

							var params = {

								RequestItems: { // A map of TableName to Put or Delete requests for that table

										Banner: Banners
									}
								};

						docClient.batchWrite(params, (err) => {
									done(err);
						});
				}
			},

				//Delete Client
				function(done){

					var paramsClient = {

							TableName: 'Client',

							Key: {
									_id: req.banner.client._id
							}
					};

					docClient.delete(paramsClient, (err)=>{

							if (err) {
								done(err);
							}else{
								res.json({message:'Client deleted'});
							}
					});

				}

			], (err) => {
				if (err) return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			});
	};

/**
* List of Banners
*/
	exports.listClient = function(req, res) {

			docClient.scan({TableName:'Client'}, (err, data) => {

				if(err){	return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});  }
					res.json(data.Items);
			});
	};

/**
* Search Banners
*/
	exports.search = function(req, res) {

				var params = {

						TableName: 'Banner',
						IndexName: 'site-status-index',
						KeyConditionExpression: 'site = :value and #status = :val',
						ExpressionAttributeNames: {
								'#status': 'status',
								'#id': '_id'
						},
						ExpressionAttributeValues: {
							':value': 'GazetaCCs',
							':val' : 1
						},
						ProjectionExpression: '#id',
					//	ExclusiveStartKey:options['ExclusiveStartKey'],
						ScanIndexForward: false,
						ReturnConsumedCapacity: 'NONE'
					};

			docClient.query(params, (err, response) => {

				if(err || !response){ res({}) ; }

				else{
				          var par = {

					          RequestItems: {

					              Article: {
					                  Keys: response.Items
					              }
					           }
				          };

							docClient.batchGet(par, (err, data) => {

										if(err){ 	res({});
										}else{
												res(data.Items);
									  }
							});
						}
			});
	};

/**
* Banner middleware
*/
	exports.bannerByID = function(req, res, next, id) {

			var params = {

				TableName: 'Client',

					Key: {
							_id : id
					}
			}
		docClient.get(params , (err, client) => {

				var banners = [];
					if (err) return next(err);
						if (! client) return next(new Error('Failed to load Banner ' + id));

							client.Item.banners.forEach((element)=> {
								 banners.push(element);
							 });

						var params = {

					    RequestItems: { // map of TableName to list of Key to get from each table

					        Banner: {

					            Keys: banners,
			            		ConsistentRead: false
					        },
					    },
					    	ReturnConsumedCapacity: 'NONE', // optional (NONE | TOTAL | INDEXES)
						};

				if(banners.length > 0){

							docClient.batchGet(params, (err, odie) => {

								if (err) return next(err);
									if (! odie) return next(new Error('Failed to load Banner ' + id));

										var response  = {

											client : client.Item,
											banners : odie.Responses.Banner
										};
											req.banner = response ;
											next();
							});

			  }else{
							var response  = {

								client : client.Item,
								banners : []
							};
								req.banner = response;
								next();
				}
		});

	};

/**
* Banner authorization middleware
*/
	exports.hasAuthorization = function(req, res, next) {

			if (req.user.roles[0]!== "admin") {
				return res.status(403).send('User is not authorized');
			}
					next();
	};

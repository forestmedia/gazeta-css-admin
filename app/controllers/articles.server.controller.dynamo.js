'use strict';

/**
* Module dependencies.
*/

var AWS = require('aws-sdk');
var	errorHandler = require('./errors.server.controller'),
	_ = require('lodash');

	AWS.config.update({
	  region: "us-east-1"
	});

var docClient = new AWS.DynamoDB.DocumentClient(); //Document Client AWS SDK to read and write data to Amazon DynamoDB
var dynamo = new AWS.DynamoDB(); //DynamoDB object
var generate = require('node-uuid');

//News's Sections
var option = [
{ label: 'Política', value: 0 },
{ label: 'Economía', value: 1 },
{ label: 'Sucesos', value: 2 },
{ label: 'Opinión', value: 3 },
{ label: 'Imagen', value: 4}
];

	//Convert title without space
	function convertToSlug(Text)
	{
	 return Text
	     .toLowerCase()
	     .replace(/[^\w ]+/g,'')
	     .replace(/ +/g,'-') ;
  }

/**
* Create Article.
*/
	exports.create = function(req, res) {

    //Convert to Number String params
		req.body.category = Number(req.body.category);
		req.body.meta.important = Number(req.body.meta.important);
    req.body.status = Number(req.body.status);

		//Create UUID to id
		req.body._id = generate.v1();

		//Convert title to Slug. Example: "this is a title" to "this-is-a-title"
		req.body.slug = convertToSlug(req.body.title);

    //Create date Unix timestamp format
		req.body.created_date = Math.floor(Date.now());

    //Create published date Unix timestap format
		if(!req.body.published_date){
			req.body.published_date = Math.floor(Date.now());
		}

	  //Create resume attribute with content with 256 characters
	  req.body.resume = req.body.content.substring(0, 256);

    //Create User attribute with user log.
		req.body.user = {
			username : req.user.username,
			displayName: req.user.displayName,
			image : req.user.image,
			meta: req.user.meta
		};

    // Dynamo params request
		var params = {
		    TableName: "Article",
            // Insert request por body to item params
		        Item: req.body
		    };

    // Create new post
		docClient.put(params, function(err, data) {
    		if (err){
            res.json(JSON.stringify(err, null, 2));
        }else{
            //Create array for batchPut to Hashtag Table
            var tagArray = [];

            //PUSH CATEGORY to array
            tagArray.push({

                PutRequest: {
                    Item: {
                      label: option[req.body.category].label.toLowerCase(),
                      _id:req.body._id,
                      published_date:req.body.published_date,
                      status:req.body.status
                    }
                }
            });

            //PUSH USER
            tagArray.push({

                PutRequest: {
                    Item: {
                      label:req.body.user.username.toLowerCase(),
                      _id:req.body._id,
                      published_date:req.body.published_date,
                      status:req.body.status
                      }
                    }
            });

            // PUSH TAGS
            for (var i = 0; i < req.body.tags.length; i++) {

                var obj = {

                  PutRequest: {

                      Item: {
                        label:req.body.tags[i].label.toLowerCase(),
                        _id:req.body._id,
                        published_date:req.body.published_date,
                        status:req.body.status
                      }
                    }
                }
                tagArray.push(obj);
          };

            //Params batchWrite tags
            var params = {
                  RequestItems: {
                      Hashtag: tagArray
                  }

            }

            docClient.batchWrite(params, function(err, data) {
                  if (err) res.json(err); // an error occurred
              });

          }
		});
      res.json({slug:req.body.slug , published_date:req.body.published_date});
	};


/**
 * Show the current Article
 */
	exports.read = function(req, res) {
			res.json(req.article);
	};

/**
* Update Article.
*/
	exports.update = function(req, res) {

    var deleteTag = req.body.deleteTag;

        delete req.body.deleteTag;

    //Dynamo params request
		var params = {

		    TableName: "Article",
		        Item: req.body
		    };

    // Create new post
		docClient.put(params, function(err, test) {

			 if (err) {
	 			return res.status(400).send({
	 				message: errorHandler.getErrorMessage(err)
	 			});

	 		}else {

				var tagArray = [];

				// PUSH TAGS
				for (var i = 0; i < req.body.tags.length; i++) {

						var obj = {

              PutRequest: {

                  Item: {
      							label:req.body.tags[i].label.toLowerCase(),
      							_id:req.body._id,
      							published_date:req.body.published_date,
      							status:req.body.status
						      }
              }
            }
						      tagArray.push(obj);
				};

        // PUSH DELETED TAGS
        for (var i = 0; i < deleteTag.length; i++) {

          var obj = {

            DeleteRequest: {

                Key: {
                    label: deleteTag[i].label.toLowerCase(),
                    published_date: deleteTag[i].published_date
                }
            }
          }
						      tagArray.push(obj);
				};

        //Params batchWrite tags
        var params = {

              RequestItems: {
                  Hashtag: tagArray
              }
        }

        docClient.batchWrite(params, function(err, data) {
              if (err) console.error(err); // an error occurred
        });

	       res.json({slug:req.body.slug , published_date:req.body.published_date});
	 		}

     });
	};

/**
* Delete Article.
*/
	exports.delete = function(req, res) {

        var params = {

            TableName: 'Article',
            Key: {
                _id: req.article._id
            }
        };

    		docClient.delete(params,function(err) {

          if (err) {
    				return res.status(400).send({
    					message: errorHandler.getErrorMessage(err)
    				});
    			}else {
    				res.json({message:'post deleted'});
    			}
    		});
	};


/**
* List of All Articles
*/
  exports.listAll = function(req, res) {

    // INIT VALUES
  	var count = req.query.count || 10;
  	var start = req.query.start || 'false';
  	var stat = Number(req.query.stat) || 0;

  	var options = {
  		limit : count,
  		ScanIndexForward: false
  	};

  	//START IF ExclusiveStartKey EXIST
  	if(start != 'false'){
  				options['ExclusiveStartKey'] = JSON.parse(req.query.start);
  	}else{
  		options['ExclusiveStartKey'] = null;
  	};

		console.log(options['ExclusiveStartKey']);

  	var params = {

  	    TableName: 'Article',
  	    IndexName: 'status-date-index',
  			KeyConditionExpression: '#status = :value',
  			Limit: count,
  	    ExpressionAttributeNames: {
  	        '#status': 'status',
  					'#id': '_id'
  	    },
  	    ExpressionAttributeValues: {
  	      ':value': stat
  	    },
  			ProjectionExpression: '#id',
  			ExclusiveStartKey:options['ExclusiveStartKey'],
  	    ScanIndexForward: false,
  	    ReturnConsumedCapacity: 'NONE'
      };

      docClient.query(params, function(err, response) {

          if(response){

            var par = {

              RequestItems: {

                Article: {
                    Keys: response.Items
                }
              }
            };

          docClient.batchGet(par, function(err, data) {

						data.Responses.Article.sort(dynamicSort("published_date"));

						var respuesta = {
              results:data.Responses.Article,
              lastKey:response.LastEvaluatedKey
            }
                if (err) res.json(err); // an error occurred
                else res.json(data.Responses.Article);
            });

          }else{
              res.json({message:"no results"});
          }
      });
  }

/**
* List of Articles
*/
	  exports.listServer = function(req, res){

	  	// INIT VALUES
	  	var count = req.query.count || 10;
	  	var start = req.query.start || 'false';

	  	var options = {
	  		limit : count,
	  		ScanIndexForward: false
	  	};

	  	//START IF ExclusiveStartKey EXIST
	  	if(start != 'false'){
	  				options['ExclusiveStartKey'] = JSON.parse(req.query.start);
	  	}else{
	  		options['ExclusiveStartKey'] = null;
	  	};


	  	var params = {

	  	    TableName: 'Article',
	  	    IndexName: 'status-date-index',
	  			KeyConditionExpression: '#status = :value',
	  			Limit: count,
	  	    ExpressionAttributeNames: {
	  	        '#status': 'status',
	  					'#id': '_id'
	  	    },
	  	    ExpressionAttributeValues: {
	  	      ':value': 3
	  	    },
	  			ProjectionExpression: '#id',
	  			ExclusiveStartKey:options['ExclusiveStartKey'],
	  	    ScanIndexForward: false,
	  	    ReturnConsumedCapacity: 'NONE'
	  	};

	  	docClient.query(params, function(err, response) {

	        if(response){

	    			var par = {
	      			RequestItems: {

	      					Article: {
	      							Keys: response.Items
	      					}
	      			}
	    			};

	  			docClient.batchGet(par, function(err, data) {
	    						if (err || !data) {res(err); }// an error occurred
	    						else {
										data.Responses.Article.sort(dynamicSort("published_date"));

											var respuesta = {
												results:data.Responses.Article,
												lastKey:response.LastEvaluatedKey
											}
										res(respuesta);
									}

	  			});

	        }else{
	            res({results:{}});
	        }
	  	});
	  };

/**
* List of Articles
*/
  exports.list = function(req, res){

  	// INIT VALUES
  	var count = req.query.count || 10;
  	var start = req.query.start || 'false';

  	var options = {
  		limit : count,
  		ScanIndexForward: false
  	};

  	//START IF ExclusiveStartKey EXIST
  	if(start != 'false'){
  				options['ExclusiveStartKey'] = JSON.parse(req.query.start);
  	}else{
  		options['ExclusiveStartKey'] = null;
  	};

  	var params = {

  	    TableName: 'Article',
  	    IndexName: 'status-date-index',
  			KeyConditionExpression: '#status = :value',
  			Limit: count,
  	    ExpressionAttributeNames: {
  	        '#status': 'status',
  					'#id': '_id'
  	    },
  	    ExpressionAttributeValues: {
  	      ':value': 3
  	    },
  			ProjectionExpression: '#id',
  			ExclusiveStartKey:options['ExclusiveStartKey'],
  	    ScanIndexForward: false,
  	    ReturnConsumedCapacity: 'NONE'
  	};

  	docClient.query(params, function(err, response) {

        if(response){

    			var par = {
      			RequestItems: {

      					Article: {
      							Keys: response.Items
      					}
      			}
    			};

  			docClient.batchGet(par, function(err, data) {

          data.Responses.Article.sort(dynamicSort("published_date"));

    				var respuesta = {

    				 	results:data.Responses.Article,
    					lastKey:response.LastEvaluatedKey
    				}
    						if (err) res.json(err); // an error occurred
    						else res.json(respuesta);
  			});

        }else{
            res.json({results:{}});
        }
  	});
  };

/**
* Article middleware
*/
exports.articleBySlug = function(req, res, next, id) {

      var params = {

        TableName: 'Article',
        IndexName: 'slug-date-index',
        KeyConditionExpression: 'slug = :value and #date = :val',
        ExpressionAttributeValues: {
          ':value': {'S': id},
          ':val' : {'N':req.query.articleDate}
        },
        ExpressionAttributeNames: {
          '#date': 'published_date'
        }
      };

      dynamo.query(params, function(err, response){

          if(err){
             res.status(404);
             res.json(err);
          }else{
            var par = {};
            par.TableName = "Article"
            var key = { _id: response.Items[0]._id.S };
            par.Key = key;

              docClient.get(par, function(err, data){
                    if(err) res.json(err);
                    req.article = data.Item;
                    next();
              });
          }
      });
};

/**
* Article authorization middleware
*/
  exports.hasAuthorization = function(req, res, next) {

  	if ((req.article.user.username !== req.user.username)&&(req.user.roles[0]!== "admin")) {
  		return res.status(403).send({
  			message: 'User is not authorized'
  		});
  	}
  	next();
  };

/**
* Custom list
*/
  exports.listCustom = function(req, res) {

      var params = {

        RequestItems: {

            Article: {
                Keys: req.query.arrayElement
            }
         }
      };

    docClient.batchGet(params, function(err, data) {
          if (err) res(err); // an error occurred
          else res(data.Responses.Article);
    });
  };

/**
* Likes list
*/
  exports.listLikes = function(req, res) {

    // INIT VALUES
    var count = req.query.count || 10;

    var params = {

        TableName: 'Article',
        IndexName: 'status-like-index',
        KeyConditionExpression: '#status = :value',
        Limit: count,
        ExpressionAttributeNames: {
            '#status': 'status',
            '#id': '_id'
        },
        ExpressionAttributeValues: {
          ':value': 3
        },
        ProjectionExpression: '#id',
        ScanIndexForward: false,
        ReturnConsumedCapacity: 'NONE'
    };

    docClient.query(params, function(err, response) {

        if(response){

              var par = {

                RequestItems: {

                    Article: {
                        Keys: response.Items
                    }
                 }
              };

            docClient.batchGet(par, function(err, data) {

                  if (err || !data) {res(err); }// an error occurred
                  else {
										data.Responses.Article.sort(dynamicSort("published_date"));

											var respuesta = {
												results:data.Responses.Article,
												lastKey:response.LastEvaluatedKey
											}
										res(respuesta);}
            });
        }else{
          res({results:{}});
        }
    });
  };

	//Sort Articles
	 function dynamicSort(property) {

			var sortOrder = -1;

				if(property[0] === "-") {
						sortOrder = 1;
						property = property.substr(1);
				}

				return function (a,b) {
						var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
						return result * sortOrder;
				}
	}
/*
var imagenes = [
  "v1467904022/dkpizbcvjqdoeepmouvf.jpg",
  "v1467575383/xzokkp4fmwvgr9k4ztwe.jpg",
  "v1467575190/p2rwfpr7pyxws2i7dkpi.jpg",
  "v1467575144/omjonlvuffklmccam49g.jpg",
  "v1467575064/zkgfkt1iqwcxylqeyiq6.jpg",
  "v1467051109/iursvnflbyrezeopwhhg.jpg",
  "v1466990771/pujmct71pct8qcogkzac.jpg",
  "v1466946884/o1p5zz8wffhochiuxs9c.jpg",
  "v1466707174/m4ebxiuyyinub7kt0swb.jpg",
  "v1466705202/caq5uxcmse2lww3x0yre.jpg",
  "v1466705154/jstlhi3ylzghqtyofk9u.jpg",
  "v1466705064/iehoybyuciglmh1g9dip.jpg",
  "v1466704997/hjuzeickdw3qmpufycw1.jpg",
  "v1466704958/exouhjbw1hmxtrmezn1a.jpg",
  "v1466704864/tatyuoyy6enq0odyzjcf.jpg",
  "v1466704808/wlivdfrdbhkxn0xpbaxs.jpg",
  "v1466704739/f9cfjulrwtdsb8z9esuk.jpg",
  "v1466704616/wztbulfuiqojwzuiogmw.jpg",
  "v1466704360/heow1fmbnircpsfcc4sc.jpg",
  "v1466704273/vlee6tgjcwliq2rglaic.jpg",
  "v1466704247/ehbf6vx8otkbk1o9kchd.jpg",
  "v1466704219/agx9zfmg5wnfsmn9trej.jpg",
  "v1466704147/eyiqn39qxj6fdzbwqcck.jpg",
  "v1466704098/xmqgyh13moetemg8xg7o.jpg"
];

var titulos = [
  "Asamblea acordó luchar y defender el verdadero Estado descentralizado",
  "Rastrean cajas de seguridad de Cristina Fernández de Kirchner",
  "Vecinos de El Junquito protestan por comida tras tres días de espera",
  "Diputado Barboza: Maduro se apoya en la FAN para desconocer a la Asamblea",
  "Eduardo Cunha renuncia a la presidencia de la Cámara Baja de Brasil",
  "Debaten sobre violaciones de DDHH contra la juventud venezolana",
  "Nepartak cada vez más cerca de Taiwán",
  "MUD arranca campaña para recolectar el 20% de firmas para activar referendo",
  "Melo: 51 empresas atienden al sector farmacéutico del país",
  "Afirman que se exportaron 18 mil metros de granito",
  "CLAP producirán 386.400 toneladas anuales de alimentos",
  "Detenidos dos escoltas de diputada por extorsión",
  "Cristiano Ronaldo va a la final para culminar su obra futbolística",
  "Alemania y Francia pelean a muerte este jueves en Marsella",
  "Brasil derribará aviones si violan el espacio aéreo durante los Olímpicos",
  'Lagarde ve "improbable" una recesión global a causa de la "Brexit"',
  'Human Rights Watch denuncia abusos policiales en Brasil',
  'La última película de Almodóvar inaugura el Festival de Cine de Jerusalén',
  'Una exposición en Tokio reinventa los acuarios tradicionales',
  'Santos anuncia que dialogará con Maduro para reapertura de frontera',
  'Diputado Duarte: las mujeres hicieron historia en la frontera'
];

var etiquetas = [
  'gazeta',
  'gzcss',
  'política',
  'economía',
  'prueba',
  'asamblea',
  'nacional',
  'sucesos',
  'estudiantes',
  'comercio',
  'primerojusticia',
  'psuv',
  'salud',
  'fuego',
  'isis',
  'brasil',
  'venezuela',
  'corrupto',
  'imaganes',
  'videos',
  'locura',
  'gente',
  'vino',
  'fogata',
  'casa',
  'perro',
  'calle',
  'vaca',
  'juegosolimpicos',
  'messi',
  'eurocopa',
  'caracas',
  'maracaibo',
  'dentista'
]




var val = 0;

nuevo();

function nuevo(){

		var intImg = imagenes[randomInt(0, imagenes.length-1)];
		var et = [];
		for (var i = 0; i < randomInt(2, 7); i++) {
		 et.push({label: etiquetas[randomInt(0, etiquetas.length-1)]})
		}

val = val + 1;
var container = { val:val, server:true, body:{}, user:
 {
   username: 'olivaresrafael',
   displayName: 'Rafael Olivares',
   image: 'v1427130764/gz/10895482_1568087676778480_1623352445_n.jpg',
   meta:
    { website: 'www.cinemalabproducers.com',
      instagram: 'akolorfilms',
      twitter: 'akolorfilms',
      facebook: 'joseluisredri',
      description: 'Esto es una prueba de perfil' } } };
container.body = {
 title: titulos[randomInt(0, titulos.length-1)],
 headlines: 'En catia los habitantes fueron atracados por ampones',
 content: '<p>adasda sdasd ad as dasd asd asdas d das a sd asd asd asd as da d dfa sdfsd fasdf as df asdf sd fa sdf asd f asdfasd fasdf asdfasdf sadf asdfasdf sd fasdf asdf asdf asdf as fas dfaf sdf a sdf sdfa sd fasd f asdf asdf sd fsdf sdf asd fasdvrgbgb xdfv aer ea </p>',
 category: randomInt(0, 4),
 tags: et,
 images:
  { url_raw: 'https://res.cloudinary.com/akolor/image/upload/'+intImg,
    url_1080: 'https://res.cloudinary.com/akolor/image/upload/c_lfill,g_auto,h_400,q_60,w_1080/'+intImg,
    url_720: 'https://res.cloudinary.com/akolor/image/upload/c_thumb,g_auto,h_407,q_60,w_720/'+intImg,
    thumb_380: 'https://res.cloudinary.com/akolor/image/upload/c_thumb,h_407,q_60,w_720/'+intImg},
 meta: { important: randomInt(0, 2), source: 'GZCss' },
 status: randomInt(0, 3),
 like: randomInt(0, 40)
  };



  createServer(container, (error, dat) =>{
		console.log(error);
		console.log(dat);
	});

   var option = [
     { label: 'Política', value: 0 },
     { label: 'Economía', value: 1 },
     { label: 'Sucesos', value: 2 },
     { label: 'Opinión', value: 3 },
     { label: 'Imagen', value: 4}
     ];

		 //Intervalo para actualziar
		 if(val <= 300){
			 setTimeout(function(){

					 nuevo();

			 },3000);
		 }


}

function randomInt (low, high) {
   return Math.floor(Math.random() * (high - low + 1) + low);
}


function createServer(req, res) {

//Convert to Number String params
req.body.category = Number(req.body.category);
req.body.meta.important = Number(req.body.meta.important);
req.body.status = Number(req.body.status);

//Create UUID to id
req.body._id = generate.v1();

//Convert title to Slug. Example: "this is a title" to "this-is-a-title"
req.body.slug = convertToSlug(req.body.title);

//Create date Unix timestamp format
req.body.created_date = Math.floor(Date.now());

//Create published date Unix timestap format
if(!req.body.published_date){
	req.body.published_date = Math.floor(Date.now());
}


	req.body.published_date = (req.val*3)+req.body.published_date;


//Create resume attribute with content with 256 characters
req.body.resume = req.body.content.substring(0, 256);

//Create User attribute with user log.
req.body.user = {
	username : req.user.username,
	displayName: req.user.displayName,
	image : req.user.image,
	meta: req.user.meta
};

// Dynamo params request
var params = {
		TableName: "Article",
				// Insert request por body to item params
				Item: req.body
		};

// Create new post
docClient.put(params, function(err, data) {
		if (err){
				res(err);
		}else{
				console.log('se creo un art');
				//Create array for batchPut to Hashtag Table
				var tagArray = [];

				//PUSH CATEGORY to array
				tagArray.push({

						PutRequest: {
								Item: {
									label: option[req.body.category].label.toLowerCase(),
									_id:req.body._id,
									published_date:req.body.published_date,
									status:req.body.status
								}
						}
				});

				//PUSH USER
				tagArray.push({

						PutRequest: {
								Item: {
									label:req.body.user.username.toLowerCase(),
									_id:req.body._id,
									published_date:req.body.published_date,
									status:req.body.status
									}
								}
				});

				// PUSH TAGS
				for (var i = 0; i < req.body.tags.length; i++) {

						var obj = {

							PutRequest: {

									Item: {
										label:req.body.tags[i].label.toLowerCase(),
										_id:req.body._id,
										published_date:req.body.published_date,
										status:req.body.status
									}
								}
						}
						if(!obj.PutRequest.Item){
							tagArray.push(obj);
						}

			};
			console.log('--------------------------------');
				//Params batchWrite tags
				var params = {
							RequestItems: {
									Hashtag: tagArray
							}

				}

				docClient.batchWrite(params, function(err, data) {
							if (err) res(err); // an error occurred
					});

			}
});

};*/

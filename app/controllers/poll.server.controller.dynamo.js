'use strict';

/**
 * Module dependencies.
 */

var AWS = require('aws-sdk');
var errorHandler = require('./errors.server.controller'),
PollSchema = require('../../app/models/poll.server.dynamo.js'),
_ = require('lodash');

AWS.config.update({
	region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient(); //Document Client AWS SDK to read and write data to Amazon DynamoDB
var dynamo = new AWS.DynamoDB(); //DynamoDB object
var generate = require('node-uuid');

/**
* GET IP ADDRESS
*/
	exports.ip = function(req,res){

		var ip = req.headers['x-forwarded-for'] ||
			req.connection.remoteAddress ||
			req.socket.remoteAddress ||
			req.connection.socket.remoteAddress;

		var params = {

			TableName: 'Vote',

	 			Key:{
					_id:req.query._id,
					ip:ip
				}
		};

		docClient.get(params, function(err, data){

				if(err) res.json(err);
				else res.json(data);
		});

	};

/**
* Create a Poll
*/
	exports.create = function(req, res) {

			req.body.status = req.body.status || 0;

			//Create UUID to id
			req.body._id = generate.v4();

			//Create date Unix timestamp format
			req.body.created_date = Math.floor(Date.now());

			//Create User attribute with user log.
			req.body.user = {
				username : req.user.username,
				displayName: req.user.displayName,
				image : req.user.image,
				meta: req.user.meta
			};

			// Dynamo params request
			var params = {

					TableName: "Poll",
							// Insert request por body to item params
							Item: req.body
					}

					docClient.put(params, function(err, data) {

								if (err) { // an error occurred
										res.status(404);
										res.json(JSON.stringify(err, null, 2));
								}
								else { // successful response
									res.json({_id:req.body._id});
								}
					});
	};

/**
* Vote Poll
*/
	exports.vote = function(req, res) {

			var ip = req.connection.remoteAddress;

			var voto = Number(req.query.textPos);

			var params = {

				TableName: 'Vote',

				Key:{
					_id:req.poll._id,
					ip:ip
				}
			};

			docClient.get(params, function(err, data){

						if(err) res.json(err);

						else{

							if (!data.Item) {

								var vote = {

									TableName: 'Vote',

											Item: {
												ip: ip,
												_id: req.poll._id,
												choice:voto
									    }

								}

					 				req.poll.options[voto].vote++;

										var voteCount = {

											TableName: 'Poll',

											Item: req.poll


										}

								docClient.put(vote, (err, data) => {
							    if (err) res.json(err); // an error occurred
							  //  else res.json(data); // successful response
								});

								docClient.put(voteCount, (err, data) => {
									if (err) res.json(err); // an error occurred
									else res.json(data); // successful response
								});

							}else{
									res.jsonp({error:true, message:"IP en lista."});
							}
						}
			});

	};

/**
* Show the current Poll
*/
	exports.read = function(req, res) {
		res.jsonp(req.poll);
	};

/**
* Update a Poll
*/
	exports.update = function(req, res) {

			// Dynamo params request
			var params = {

					TableName: "Poll",
							// Insert request por body to item params
							Item: req.body
			}

				docClient.put(params, function(err, data) {

						if (err) { // an error occurred
								res.status(404);
								res.json(JSON.stringify(err, null, 2));
						}
						else { // successful response
							res.json({_id: req.body._id});
						}
				});
	};

/**
* Delete a Poll
*/
	exports.delete = function(req, res) {

			var params = {
					TableName: 'Poll',
					Key: {
							_id: req.poll._id
					}

		 	};

				docClient.delete(params,function(err) {
					if (err) {

						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json({message:'poll deleted'});
					}
				});
	};

/**
* List of Poll
*/
	exports.list = function(req, res) {

			var stat = req.query.stat || 0;
			var count = req.query.count || 10;
			var start = req.query.start || 'false';
			var options = {
				limit : count,
				ScanIndexForward: false
			};

			//START IF ExclusiveStartKey EXIST
			if(start != 'false'){
						options['ExclusiveStartKey'] = JSON.parse(req.query.start);
			}else{
				options['ExclusiveStartKey'] = null;
			};

			var params = {

					TableName: 'Poll',
					IndexName: 'poll-index',
					KeyConditionExpression: '#status = :value',
					Limit: count,
					ExpressionAttributeNames: {
							'#status': 'status',
							'#id': '_id'
					},
					ExpressionAttributeValues: {
						':value': stat
					},
					ProjectionExpression: '#id',
					ExclusiveStartKey:options['ExclusiveStartKey'],
					ScanIndexForward: false,
					ReturnConsumedCapacity: 'NONE'
			};


				docClient.query(params, function(err, response) {

							if(response){
								var par = {
								RequestItems: {
										Poll: {
												Keys: response.Items
										}
								 }
								};

								docClient.batchGet(par, function(err, data) {
									if(err) res.json(err);
									else res.json(data.Responses);

							});
							}else{

									res.json({message:"no results"});
							}
				});

	};

/**
* Poll middleware
*/
exports.pollByID = function(req, res, next, id) {

		var params = {
			TableName: 'Poll',
 			Key:{
				_id:id
			}
		};

		docClient.get(params, function(err, data){

				if(err) res.json(err);
				req.poll = data.Item;
				next();
		});

};

/**
* Poll middleware (Server)
*/
	exports.pollByServer = function(req, res) {

		var params = {
			TableName: 'Poll',
			Key:{
				_id:req.query._id
			}
		};
		docClient.get(params, function(err, data){

				if(err) res(err);
				else res(data.Item);
		});
	};

/**
* Poll authorization middleware
*/
	exports.hasAuthorization = function(req, res, next) {
		if (req.poll.user.username !== req.user.username || req.user.roles[0]!== "admin") {
			return res.status(403).send('User is not authorized');
		}
		next();
	};

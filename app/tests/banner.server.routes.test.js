'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Banner = mongoose.model('Banner'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, banner;

/**
 * Banner routes tests
 */
describe('Banner CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Banner
		user.save(function() {
			banner = {
				name: 'Banner Name'
			};

			done();
		});
	});

	it('should be able to save Banner instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Banner
				agent.post('/banners')
					.send(banner)
					.expect(200)
					.end(function(bannerSaveErr, bannerSaveRes) {
						// Handle Banner save error
						if (bannerSaveErr) done(bannerSaveErr);

						// Get a list of Banners
						agent.get('/banners')
							.end(function(bannersGetErr, bannersGetRes) {
								// Handle Banner save error
								if (bannersGetErr) done(bannersGetErr);

								// Get Banners list
								var banners = bannersGetRes.body;

								// Set assertions
								(banners[0].user._id).should.equal(userId);
								(banners[0].name).should.match('Banner Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Banner instance if not logged in', function(done) {
		agent.post('/banners')
			.send(banner)
			.expect(401)
			.end(function(bannerSaveErr, bannerSaveRes) {
				// Call the assertion callback
				done(bannerSaveErr);
			});
	});

	it('should not be able to save Banner instance if no name is provided', function(done) {
		// Invalidate name field
		banner.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Banner
				agent.post('/banners')
					.send(banner)
					.expect(400)
					.end(function(bannerSaveErr, bannerSaveRes) {
						// Set message assertion
						(bannerSaveRes.body.message).should.match('Please fill Banner name');
						
						// Handle Banner save error
						done(bannerSaveErr);
					});
			});
	});

	it('should be able to update Banner instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Banner
				agent.post('/banners')
					.send(banner)
					.expect(200)
					.end(function(bannerSaveErr, bannerSaveRes) {
						// Handle Banner save error
						if (bannerSaveErr) done(bannerSaveErr);

						// Update Banner name
						banner.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Banner
						agent.put('/banners/' + bannerSaveRes.body._id)
							.send(banner)
							.expect(200)
							.end(function(bannerUpdateErr, bannerUpdateRes) {
								// Handle Banner update error
								if (bannerUpdateErr) done(bannerUpdateErr);

								// Set assertions
								(bannerUpdateRes.body._id).should.equal(bannerSaveRes.body._id);
								(bannerUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Banners if not signed in', function(done) {
		// Create new Banner model instance
		var bannerObj = new Banner(banner);

		// Save the Banner
		bannerObj.save(function() {
			// Request Banners
			request(app).get('/banners')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Banner if not signed in', function(done) {
		// Create new Banner model instance
		var bannerObj = new Banner(banner);

		// Save the Banner
		bannerObj.save(function() {
			request(app).get('/banners/' + bannerObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', banner.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Banner instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Banner
				agent.post('/banners')
					.send(banner)
					.expect(200)
					.end(function(bannerSaveErr, bannerSaveRes) {
						// Handle Banner save error
						if (bannerSaveErr) done(bannerSaveErr);

						// Delete existing Banner
						agent.delete('/banners/' + bannerSaveRes.body._id)
							.send(banner)
							.expect(200)
							.end(function(bannerDeleteErr, bannerDeleteRes) {
								// Handle Banner error error
								if (bannerDeleteErr) done(bannerDeleteErr);

								// Set assertions
								(bannerDeleteRes.body._id).should.equal(bannerSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Banner instance if not signed in', function(done) {
		// Set Banner user 
		banner.user = user;

		// Create new Banner model instance
		var bannerObj = new Banner(banner);

		// Save the Banner
		bannerObj.save(function() {
			// Try deleting Banner
			request(app).delete('/banners/' + bannerObj._id)
			.expect(401)
			.end(function(bannerDeleteErr, bannerDeleteRes) {
				// Set message assertion
				(bannerDeleteRes.body.message).should.match('User is not logged in');

				// Handle Banner error error
				done(bannerDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Banner.remove().exec();
		done();
	});
});
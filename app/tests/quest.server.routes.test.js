'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Quest = mongoose.model('Quest'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, quest;

/**
 * Quest routes tests
 */
describe('Quest CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Quest
		user.save(function() {
			quest = {
				name: 'Quest Name'
			};

			done();
		});
	});

	it('should be able to save Quest instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Quest
				agent.post('/quests')
					.send(quest)
					.expect(200)
					.end(function(questSaveErr, questSaveRes) {
						// Handle Quest save error
						if (questSaveErr) done(questSaveErr);

						// Get a list of Quests
						agent.get('/quests')
							.end(function(questsGetErr, questsGetRes) {
								// Handle Quest save error
								if (questsGetErr) done(questsGetErr);

								// Get Quests list
								var quests = questsGetRes.body;

								// Set assertions
								(quests[0].user._id).should.equal(userId);
								(quests[0].name).should.match('Quest Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Quest instance if not logged in', function(done) {
		agent.post('/quests')
			.send(quest)
			.expect(401)
			.end(function(questSaveErr, questSaveRes) {
				// Call the assertion callback
				done(questSaveErr);
			});
	});

	it('should not be able to save Quest instance if no name is provided', function(done) {
		// Invalidate name field
		quest.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Quest
				agent.post('/quests')
					.send(quest)
					.expect(400)
					.end(function(questSaveErr, questSaveRes) {
						// Set message assertion
						(questSaveRes.body.message).should.match('Please fill Quest name');
						
						// Handle Quest save error
						done(questSaveErr);
					});
			});
	});

	it('should be able to update Quest instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Quest
				agent.post('/quests')
					.send(quest)
					.expect(200)
					.end(function(questSaveErr, questSaveRes) {
						// Handle Quest save error
						if (questSaveErr) done(questSaveErr);

						// Update Quest name
						quest.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Quest
						agent.put('/quests/' + questSaveRes.body._id)
							.send(quest)
							.expect(200)
							.end(function(questUpdateErr, questUpdateRes) {
								// Handle Quest update error
								if (questUpdateErr) done(questUpdateErr);

								// Set assertions
								(questUpdateRes.body._id).should.equal(questSaveRes.body._id);
								(questUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Quests if not signed in', function(done) {
		// Create new Quest model instance
		var questObj = new Quest(quest);

		// Save the Quest
		questObj.save(function() {
			// Request Quests
			request(app).get('/quests')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Quest if not signed in', function(done) {
		// Create new Quest model instance
		var questObj = new Quest(quest);

		// Save the Quest
		questObj.save(function() {
			request(app).get('/quests/' + questObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', quest.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Quest instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Quest
				agent.post('/quests')
					.send(quest)
					.expect(200)
					.end(function(questSaveErr, questSaveRes) {
						// Handle Quest save error
						if (questSaveErr) done(questSaveErr);

						// Delete existing Quest
						agent.delete('/quests/' + questSaveRes.body._id)
							.send(quest)
							.expect(200)
							.end(function(questDeleteErr, questDeleteRes) {
								// Handle Quest error error
								if (questDeleteErr) done(questDeleteErr);

								// Set assertions
								(questDeleteRes.body._id).should.equal(questSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Quest instance if not signed in', function(done) {
		// Set Quest user 
		quest.user = user;

		// Create new Quest model instance
		var questObj = new Quest(quest);

		// Save the Quest
		questObj.save(function() {
			// Try deleting Quest
			request(app).delete('/quests/' + questObj._id)
			.expect(401)
			.end(function(questDeleteErr, questDeleteRes) {
				// Set message assertion
				(questDeleteRes.body.message).should.match('User is not logged in');

				// Handle Quest error error
				done(questDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Quest.remove().exec();
		done();
	});
});
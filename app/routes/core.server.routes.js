'use strict';



module.exports = function(app) {
	// Root routing
	var users = require('../../app/controllers/users.server.controller.dynamo'),
	core = require('../../app/controllers/core.server.controller'),
	cache = require('../../app/controllers/create.server.controller');



	app.route('/').get( core.index);
	app.route('/model').get( cache.model);

	app.route('/images')
		.get(core.images);
	app.route('/twitter')
		.get(core.mensajes);
	app.route('/image')
		.post(users.requiresLogin, core.image);
	app.route('/videos/:idp')
		.get(core.read);
	app.route('/vhome/:videoCount')
		.get(core.read);
	app.route('/settingsfile')
		.put(users.requiresLogin, cache.settingsfile);


	app.param('idp', core.videos);
	app.param('videoCount', core.hvideos);



};

var AWS = require('aws-sdk');

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB();

//Check if Table exists
docClient.describeTable({TableName: 'Poll'}, function(err) {
    if (!err) console.log("Poll Table Exists"); // an error occurred

    else{
      //Create Poll Table
      var params = {

          TableName : "Poll",

          KeySchema: [
              { AttributeName: "_id", KeyType: "HASH" } //Partition key
              ],

                  AttributeDefinitions: [
                      { AttributeName: "_id", AttributeType: "S" },
                      { AttributeName: "created_date", AttributeType: "N" },
                      { AttributeName: "status", AttributeType: "N" }],

          GlobalSecondaryIndexes: [

              {
              IndexName: "poll-index",
                  KeySchema: [
                  {AttributeName: "status", KeyType: "HASH"}, //Partition key
                  {AttributeName: "created_date", KeyType: "RANGE"}, //Sort key
                  ],
                      Projection: {
                      "ProjectionType": "KEYS_ONLY"
                      },
                      ProvisionedThroughput: {
                      "ReadCapacityUnits": 5,"WriteCapacityUnits": 1
                      }
              }],

          ProvisionedThroughput: {
          ReadCapacityUnits: 5,
          WriteCapacityUnits: 1
          }
      };

      docClient.createTable(params, function(err, data) {
          if (err) console.error(err); // an error occurred
          else console.log(data); // successful response

      });

    }
})

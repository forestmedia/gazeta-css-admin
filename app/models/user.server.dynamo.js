var AWS = require('aws-sdk');
var crypto = require('crypto');


AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB();

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
	return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
	return (this.provider !== 'local' || (password && password.length > 6));
};


//Check if Table exists
docClient.describeTable({TableName: 'User'}, function(err) {
    if (!err) console.log("User Table Exists"); // an error occurred

    else{

      //Create User Table
      var params = {
          TableName: 'User',
          KeySchema: [
              { AttributeName: 'username', KeyType: 'HASH'
              }
          ],
          AttributeDefinitions: [
              {
                  AttributeName: 'username',
                  AttributeType: 'S',
              }
          ],
          ProvisionedThroughput: {
              ReadCapacityUnits: 1,
              WriteCapacityUnits: 1,
          }
      };

      docClient.createTable(params, function(err, data) {
          if (err) console.error(err); // an error occurred
          else console.log(data); // successful response

      });

    }
})

/**
 * Hook a pre save method to hash the password
 */
exports.pre = function(password, done) {
	if (password && password.length > 6) {
		var salt = new Buffer(crypto.randomBytes(16)).toString();
		password = exports.hashPassword(password, salt);
    return {salt:salt, password:password};
	}
};

/**
 * Create instance method for hashing a password
 */

 exports.hashPassword = function(password, salt) {
 	if (salt && password) {
 	 return crypto.pbkdf2Sync(password, salt, 10000, 64 , 'sha256' ).toString('base64');
 	} else {
 		return password;
 	}
 };

/**
 * Create instance method for authenticating user
 */
exports.authenticate = function(password, user) {
	return user.password === exports.hashPassword(password, user.salt);
};

/**
 * Find possible not used username
 */
exports.findUniqueUsername = function(username, suffix, callback) {
	var _this = this;
	var possibleUsername = username + (suffix || '');

	_this.queryOne({
		username: possibleUsername
	}, function(err, user) {
		if (!err) {
			if (!user) {
				callback(possibleUsername);
			} else {
				return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
			}
		} else {
			callback(null);
		}
	});
};

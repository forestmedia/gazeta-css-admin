var AWS = require('aws-sdk');

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB();

//Check if Table exists
docClient.describeTable({TableName: 'Article'}, function(err) {
    if (!err) console.log("Article Table Exists"); // an error occurred

    else{
      //Create Article Table
      var params = {

          TableName : "Article",

          KeySchema: [
              { AttributeName: "_id", KeyType: "HASH" } //Partition key
              ],

                  AttributeDefinitions: [
                      { AttributeName: "_id", AttributeType: "S" },
                      { AttributeName: "published_date", AttributeType: "N" },
                      { AttributeName: "status", AttributeType: "N" },
                      { AttributeName: "slug", AttributeType: "S" },
                      { AttributeName: "like", AttributeType: "N" }],

          GlobalSecondaryIndexes: [

              {
              IndexName: "slug-date-index",
                  KeySchema: [
                  {AttributeName: "slug", KeyType: "HASH"}, //Partition key
                  {AttributeName: "published_date", KeyType: "RANGE"}, //Sort key
                  ],
                      Projection: {
                      "ProjectionType": "KEYS_ONLY"
                      },
                      ProvisionedThroughput: {
                      "ReadCapacityUnits": 5,"WriteCapacityUnits": 1
                      }
              },

              {
              IndexName: "status-date-index",
                  KeySchema: [
                  {AttributeName: "status", KeyType: "HASH"}, //Partition key
                  {AttributeName: "published_date", KeyType: "RANGE"}, //Sort key
                  ],
                      Projection: {
                      "ProjectionType": "KEYS_ONLY"
                      },
                      ProvisionedThroughput: {
                      "ReadCapacityUnits": 5,"WriteCapacityUnits": 1
                      }
              },

              {
              IndexName: "status-like-index",
                  KeySchema: [
                  {AttributeName: "status", KeyType: "HASH"}, //Partition key
                  {AttributeName: "like", KeyType: "RANGE"}, //Sort key
                  ],
                      Projection: {
                      "ProjectionType": "KEYS_ONLY"
                      },
                      ProvisionedThroughput: {
                      "ReadCapacityUnits": 5,"WriteCapacityUnits": 1
                      }
              }],

          ProvisionedThroughput: {
          ReadCapacityUnits: 5,
          WriteCapacityUnits: 1
          }
      };

      docClient.createTable(params, function(err, data) {
          if (err) console.error(err); // an error occurred
          else console.log(data); // successful response

      });

    }
})

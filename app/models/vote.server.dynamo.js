var AWS = require('aws-sdk');

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB();

//Check if Table exists
docClient.describeTable({TableName: 'Vote'}, function(err) {
    if (!err) console.log("Vote Table Exists"); // an error occurred

    else{
      //Create Vote Table
      var params = {

          TableName : "Vote",

          KeySchema: [
              { AttributeName: "_id", KeyType: "HASH" }, //Partition key
              { AttributeName: "ip", KeyType: "RANGE" }
              ],

                  AttributeDefinitions: [
                      { AttributeName: "_id", AttributeType: "S" },
                      { AttributeName: "ip", AttributeType: "S" }],

          ProvisionedThroughput: {
          ReadCapacityUnits: 5,
          WriteCapacityUnits: 1
          }
      };

      docClient.createTable(params, function(err, data) {
          if (err) console.error(err); // an error occurred
          else console.log(data); // successful response

      });

    }
})

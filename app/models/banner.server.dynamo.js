  var AWS = require('aws-sdk');
  AWS.config.update({
	  region: "us-east-1"
	});

 	var docClient = new AWS.DynamoDB();


  var paramsBanner = {
      TableName : "Banner",

      KeySchema: [
          { AttributeName: "_id", KeyType: "HASH" } //Partition key
          ],

              AttributeDefinitions: [
                  { AttributeName: "_id", AttributeType: "S" },

                  { AttributeName: "status", AttributeType: "N" },
                  { AttributeName: "site", AttributeType: "S" }],

      GlobalSecondaryIndexes: [

          {
          IndexName: "site-status-index",
              KeySchema: [
              {AttributeName: "site", KeyType: "HASH"}, //Partition key
              {AttributeName: "status", KeyType: "RANGE"}, //Sort key
              ],
                  Projection: {
                  "ProjectionType": "KEYS_ONLY"
                  },
                  ProvisionedThroughput: {
                  "ReadCapacityUnits": 2,"WriteCapacityUnits": 1
                  }
          }],

      ProvisionedThroughput: {
      ReadCapacityUnits: 2,
      WriteCapacityUnits: 1
      }
  };

  var paramsClient = {

   TableName : "Client",

   KeySchema: [
       { AttributeName: "_id", KeyType: "HASH" } //Partition key
       ],

   AttributeDefinitions: [
       { AttributeName: "_id", AttributeType: "S" }
     ],

   ProvisionedThroughput: {
   ReadCapacityUnits: 1,
   WriteCapacityUnits: 1
   }
};

  //Check if Table exists
  docClient.describeTable({TableName: 'Banner'}, (err) => {

      if (!err) {console.log("Banner Table Exists");
        }else{ // an error occurred
        docClient.createTable(paramsBanner, (err, data) => {
          if (err) console.error(err);// an error occurred
          else console.log(data);// successful response
        });
    }
});

//Check if Table exists
docClient.describeTable({TableName: 'Client'}, (err) => {

    if (!err) {console.log("Client Table Exists");
      }else{ // an error occurred
      docClient.createTable(paramsClient, (err, data) => {
          if (err) console.error(err);// an error occurred
          else console.log(data);// successful response

      });
    }

});

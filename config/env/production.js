'use strict';

module.exports = {
	assets: {
		lib: {
			css: [
				'public/dist/vendor.min.css'
			],
			js: [
				'public/dist/vendor.min.js'
			]
		},
		css:[
			'public/dist/application.min.css'
		],
		js: [
			'public/dist/application.min.js'
		]

	},
	mailer: {
		from: process.env.MAILER_FROM || 'anto2318@gmail.com',
		options: {
			port: 465,
			secure: true, // use SSL
			host: process.env.MAILER_SERVICE_PROVIDER || 'smtp.gmail.com',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'anto2318',
				pass: process.env.MAILER_PASSWORD || 'viajes4000'
			}
		}
	}
};

/*
 * angular-number-format: A simple AngularJS module that formats number in 'input' HTML tag.
 * Authors: SINN Huiseong at akaiv, inc. (https://github.com/lumiere0b/angular-number-format/blob/master/README.md)
 * License: MIT (http://www.opensource.org/licenses/mit-license.php)
 * Version: 0.1-20140110
 */
(function (angular) {
	var app = angular.module("lumiere0b.number-format", []);
	app.directive("numberFormat", ["$filter", function ($filter) {
		return {
			restrict: "A",
			require: "?ngModel",
			link: function (scope, elem, attrs, ngModel) {
				if (! ngModel) return;

				/*
				elem.on("blur keyup change", function () {
					scope.$apply(refreshValue);
				});
				*/
				scope.$watch(attrs.ngModel, refreshValue, true);

				function refreshValue() {
					var val = elem.val();
					var replaced = parseInt(val.replace(/[^0-9]/g, ""), 0);
					ngModel.$setViewValue(replaced);
					elem.val($filter("number")(replaced));
				}
			}
		}
	}]);
})(angular);

'use strict';

// Banners controller
angular.module('banners').controller('BannersController', ['$scope', '$stateParams', '$location', 'Authentication', 'Banners','$uibModal', '$rootScope',
	function($scope, $stateParams, $location, Authentication, Banners, $uibModal, $rootScope) {
		$scope.authentication = Authentication;


		$scope.opPos = [
			{label:"Light", value:0},
			{label:"Standard", value:1},
			{label:"Platinum", value:2},
			{label:"Premium", value:3}
		];

		$scope.opPage = [
			{label:"300x250", width:300, height:250 , value:0},
			{label:"336x280", width:366, height:280 , value:1},
			{label:"728x90", width:728, height:90 , value:2},
			{label:"300x600", width:300, height:600 , value:3},
			{label:"320x100", width:320, height:100 , value:4}
		];

	var current  = Number;
		//MODAL
		$scope.galeria = function (indice) {

		current = indice;
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'modules/articles/views/image-gallery.client.view.html',
      controller: 'ArticlesGallery',
      size: 'lg'
    });
}


$scope.$on ("imageReady",function (event, data) {

		 if (data.index != undefined) {
			 $scope.banners[data.index].image.banner = data.url;
		 }else{
			 $scope.banners[current].image.banner = data;
		 }


});


		var removed = [];

		$scope.removeBanner = function(banner){

			for (var i in $scope.banners) {
				if ($scope.banners[i] === banner) {
					$scope.banners.splice(i, 1);
				}
			}

			var obj = {
				_id : banner._id
			};
			if(banner._id){removed.push(obj);console.log(removed);}

		}


		$scope.inicializar = function(){

			$scope.client = {};

			$scope.banners =[];
}

		$scope.newBanner = function(){
				  var nuevo = {
						image:{
							banner:""
						}
					};
						$scope.banners.push(nuevo);
		}

		// Create new Banner
		$scope.create = function() {
			// Create new Banner object
			$scope.banners.expires = new Date($scope.banners.expires);
			var banner = new Banners ({
				client: $scope.client,
				banners: $scope.banners
			});

			// Redirect after save
			banner.$save(function(response) {
				$location.path('banners/' + response._id +  "/edit");
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Banner
		$scope.remove = function(client) {
			console.log(client);
			if ( client ) {
				Banners.delete({
					bannerId: client._id
				},function() {
					$location.path('banners/list');
				});

				for (var i in $scope.clients) {
					if ($scope.clients [i] === client) {
						$scope.clients.splice(i, 1);
					}
				}
			} else {
				$scope.banner.$remove({
					bannerId: $stateParams.bannerId,
				}, function() {
					$location.path('banners');
				});
			}
		};

		// Update existing Banner
		$scope.update = function() {


			var todo = new Banners ({
				client: $scope.client,
				banners: $scope.banners,
				removed: removed
			});
			todo.$update(function(res) {
				$location.path('banners/' + res._id + "/edit");
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Banners
		$scope.find = function() {
			Banners.query( function(response){
				$scope.clients = response;
			});

		};

		// Find existing Banner
		$scope.findOne = function() {
			Banners.get({
				bannerId: $stateParams.bannerId
			}, function(response){

				response.banners[0].expires = new Date(response.banners[0].expires);
				$scope.client = response.client;
				$scope.banners = response.banners;

				});
		};
	}
]);

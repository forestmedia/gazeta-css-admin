'use strict';

//Polls service used to communicate Polls REST endpoints
angular.module('core').factory('Polls', ['$resource',
	function($resource) {
		return $resource('polls/:pollId', { pollId: '@_id'
		}, {
			update: {
				url:'polls/:pollId',
				method: 'PUT'
			}
		},
		{
			update: {
				url:'polls/:pollId',
				method: 'POST'
			}
		},
		{
 			query: {
 			method: 'GET',
 			params: { pollId: 'polls' },
			cache: true,
 			ignoreLoadingBar: true
 		}
 	});
	}
]);

angular.module('core').factory('Vote', ['$resource',
	function($resource) {
		return $resource('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/vote/:pollId', { pollId: '@_id'
		} ,
		{
 			save: {
 			method: 'POST',
 			params: { pollId: 'polls'},
 			ignoreLoadingBar: true
 		}
 	});
	}
]);

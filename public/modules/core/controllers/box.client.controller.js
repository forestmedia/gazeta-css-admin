angular.module('core').controller('box-controller', ['$scope' ,
	function($scope) {
		$scope.option = [
	    { label: 'Política', value: 0 },
	    { label: 'Economía', value: 1 },
	    { label: 'Sucesos', value: 2 },
	    { label: 'Opinión', value: 3 },
	    { label: 'Imagen del día', value: 4 }
	  	];
	  }]);

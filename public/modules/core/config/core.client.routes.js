'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider','cfpLoadingBarProvider',


	function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {

		cfpLoadingBarProvider.includeBar = true;

		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html',
			controller:'AuthenticationController'
		});
		$urlRouterProvider.otherwise('/');
	}
]);

angular.module('core').run(['amMoment','$rootScope', '$http' ,function(amMoment, $rootScope, $http) {
    amMoment.changeLocale('es');

		$rootScope.option = [
			  { label: 'Política', value: 0 },
			  { label: 'Economía', value: 1 },
			  { label: 'Sucesos', value: 2 },
			  { label: 'Opinión', value: 3 },
				{ label: 'Imagen', value: 4}
			];


}]);

angular.module('core').filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

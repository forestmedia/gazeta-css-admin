'use strict';

// Configuring the Articles module
angular.module('polls').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Encuestas', 'polls/list', '/polls(/create)?');
	}
]);

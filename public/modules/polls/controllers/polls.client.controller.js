'use strict';

// Polls controller
angular.module('polls').controller('PollsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Polls',
	function($scope, $stateParams, $location, Authentication, Polls) {
		$scope.authentication = Authentication;

		// Create new Poll
		$scope.create = function() {
			// Create new Poll object
			var poll = new Polls ({
				question: this.question,
				options: $scope.options
			});

			// Redirect after save
			poll.$save(function(response) {
				$location.path('polls/' + response._id);

				// Clear form fields
				$scope.question = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Poll
		$scope.remove = function(poll) {

			if (poll) {
				Polls.delete({
					pollId: poll._id
				},function() {
					$location.path('polls/list');
				});

				for (var i in $scope.polls) {
					if ($scope.polls[i] === poll) {
						$scope.polls.splice(i, 1);
					}
				}
			} else {
				$scope.poll.$remove({
					pollId: $stateParams.pollId
				}, function() {
					$location.path('polls/list');
				});
			}

		};

		// Update existing Poll
		$scope.update = function() {
			var poll = $scope.poll;

			poll.options = $scope.options;

			poll.$update(function() {
				$location.path('polls/' + poll._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Polls
		$scope.find = function() {
			Polls.get(function(response){
				console.log(response);
				$scope.polls = response.Poll;
			});
		};


		// Find existing Poll
		$scope.findOne = function() {
			$scope.options = [];
			$scope.poll = Polls.get({
				pollId: $stateParams.pollId
			}, function(data){

				$scope.options = data.options;

			});

		};

		var indice = undefined;
		$scope.inicializar = function(){
			$scope.options =[];
			indice = 0;
		};



		$scope.addOpt = function(){

			var dato = {text : $scope.dataOpt, value: indice, vote:0};
			$scope.options.push(dato);
			console.log($scope.options);
			$scope.dataOpt = "";
			indice++;
		};

		$scope.removeOpt = function(ind){
			$scope.options.splice(ind,1);
		}

	}
]);

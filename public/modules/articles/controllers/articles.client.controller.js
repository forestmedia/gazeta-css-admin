'use strict';

angular.module('articles').controller('ArticlesController', ['$scope', '$stateParams', '$location','$rootScope', 'Authentication', 'Articles','$http' ,'Etiquetas','$uibModal','$timeout',
	function($scope, $stateParams, $location,$rootScope ,Authentication, Articles, $http , Etiquetas, $uibModal, $timeout) {
		$scope.authentication = Authentication;

		$scope.fecha = new Date();

		$scope.tooltipShow = function(inx){
			$scope.articles[inx].tooltipIsOpen = !$scope.articles[inx].tooltipIsOpen;
			$timeout(function () {
				$scope.articles[inx].tooltipIsOpen = !$scope.articles[inx].tooltipIsOpen;
			}, 2000);
		}



		$scope.$on('decipher.tags.removed', function(event, data) {

			if (!$scope.article) {
					delete data.tag;
			}

			else {
				if($scope.article.deleteTag != undefined){
						var tag = {
							label: data.tag.label,
							published_date: $scope.article.published_date
						}
						 $scope.article.deleteTag.push(tag);
						 console.log($scope.article.deleteTag);
				 }
			}


		});

		//MODAL
		$scope.galeria = function () {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'modules/articles/views/image-gallery.client.view.html',
      controller: 'ArticlesGallery',
      size: 'lg'
    });

  };

		$scope.options = [
	    { label: 'Política', value: 0 },
	    { label: 'Economía', value: 1 },
	    { label: 'Seguridad', value: 2 },
	    { label: 'Opinión', value: 3 },
	    { label: 'Imagen del día', value: 4 }

	  	];


  	    $rootScope.$on ("cfpLoadingBar:loading",function (event, data) {
      		 $scope.cargado = false;
     			 return
     		});

		    $rootScope.$on ("cfpLoadingBar:completed",function (event, data) {
		     	$scope.cargado = true;
		        return
		    });

				$rootScope.$on ("imageReady",function (event, data) {
					$scope.images = data;
					if($scope.article){$scope.article.images = data;}

				});


	  	$scope.typeaheadOpts = {
			delimiter : ' ',
		  minLength: 3,
		  waitMs: 500,
		  allowsEditable: true
		};

		$scope.inicializar = function() {

		$scope.tags =  [{label:'gazeta'}, {label:'noticia'}];
		$scope.images  = { url_raw:"",url_1080:"",url_720:"", thumb_380:""};
		$scope.meta = {
			important:0,
			source:'GZCss'
		};
		$scope.status = 0;
		$scope.category = $scope.options[0];
		$scope.states = new Etiquetas();

		};

		var cuenta = 8;

		var params = {
			page: 1,
			count: 8

		};

		$scope.create = function() {
			var article = new Articles({
				title: this.title,
				headlines: this.headlines,
				content: this.content,
				category: this.category.value,
				tags: this.tags,
				images: this.images,
				meta: this.meta,
				status: this.status
			});
			article.$save(function(response) {
				$location.path('articles/' + response.slug + '/' + response.published_date);

				$scope.title = '';
				$scope.headlines = '';
				$scope.content = '';
				$scope.images = {};
				$scope.category = 0;
				$scope.tags = [];
				$scope.meta = {
					important:0,
					source:'GZCss'
				};
				$scope.status = 0;

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};


		$scope.remove = function(article) {
			if (article) {
				Articles.delete({
					articleId: article.slug,
					articleDate: article.published_date
				},function() {
					$location.path('articles/list');
				});

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove({
					articleId: $stateParams.articleId,
					articleDate: $stateParams.articleDate
				}, function() {
					$location.path('articles/list');
				});
			}
		};

		$scope.update = function() {

			var article = $scope.article;

			article.category = $scope.category.value;
			article.$update({
				articleId: $stateParams.articleId,
				articleDate: $stateParams.articleDate
			}, function() {
				$location.path('articles/' + article.slug + '/' + article.published_date);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.play = function(){

			$scope.verPic = !$scope.verPic;
		}




		$scope.find = function(cuenta) {


			params.count = cuenta;


			Articles.get(params, function(response){

					$scope.articles = response.results;

			});


		};

		$scope.findT = function(int) {
			var stat = int || 0;
			var url = "/articlesall?stat="+stat;
			$http.get(url).then(  function(response){
				$scope.articles = response.data;
			});
		};

		$scope.findOne = function() {

			$scope.states = new Etiquetas();
			$scope.article = Articles.get({
				articleId: $stateParams.articleId,
				articleDate: $stateParams.articleDate
			}, function(response){
				$scope.article.deleteTag = [];
				$rootScope.$broadcast("imageReady", response.images);
				$scope.category = $scope.options[response.category];
				if(!response.tags){$scope.article.tags = [{label:'gazeta'}, {label:'noticia'}];}

				});
		};
	}])

.controller('modalUploaderController', function($scope, FileUploader, $uibModalInstance) {
					var uploader = $scope.uploader = new FileUploader({
							url: 'image',
							autoUpload: true
					});

					$scope.img = {
						url: ''
					};
					$scope.submit = function() {
						console.log($scope.img.url);
						$uibModalInstance.close($scope.img.url);
					};

					// FILTERS
					uploader.filters.push({
							name: 'imageFilter',
							fn: function(item /*{File|FileLikeObject}*/, options) {
									var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
									return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
							}
					},
					{
								name: 'customFilter',
								fn: function(item /*{File|FileLikeObject}*/, options) {
										return this.queue.length < 1;
								}
						});

					// CALLBACKS

					uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
							console.info('onWhenAddingFileFailed', item, filter, options);
					};
					uploader.onAfterAddingFile = function(fileItem) {
							console.info('onAfterAddingFile', fileItem);
					};
					uploader.onAfterAddingAll = function(addedFileItems) {
							console.info('onAfterAddingAll', addedFileItems);
					};
					uploader.onBeforeUploadItem = function(item) {
							console.info('onBeforeUploadItem', item);
					};
					uploader.onProgressItem = function(fileItem, progress) {
							console.info('onProgressItem', fileItem, progress);
					};
					uploader.onProgressAll = function(progress) {
							console.info('onProgressAll', progress);
					};
					uploader.onSuccessItem = function(fileItem, response, status, headers) {
							console.info('onSuccessItem', fileItem, response, status, headers);
					};
					uploader.onErrorItem = function(fileItem, response, status, headers) {
							console.info('onErrorItem', fileItem, response, status, headers);
					};
					uploader.onCancelItem = function(fileItem, response, status, headers) {

							console.info('onCancelItem', fileItem, response, status, headers);
					};
					uploader.onCompleteItem = function(fileItem, response, status, headers) {

						$scope.img.url = response.url_720;


						console.info('onCompleteItem', fileItem, response, status, headers);

					};
					uploader.onCompleteAll = function() {
							console.info('onCompleteAll');
					};

					console.info('uploader', uploader);
			});

// Controlador Galeria
	angular.module('articles').controller('ArticlesGallery', ['$scope', '$http','$uibModalInstance','$rootScope','$location',
		function($scope, $http, $uibModalInstance, $rootScope, $location) {

			var ban = $location.path().substring(1,8);

			if (ban == 'banners') {
					var url = "/images?tag=banner";
			}else{
						var url = "/images";
			}

			$http.get(url).then( function(response){

					$scope.imagenes = response.data.results.resources;
					$scope.tags = response.data.tags.tags;
					$scope.next =  response.data.results.next_cursor;
			});


	var config = {ignoreLoadingBar: true};

	$rootScope.$on ("cfpLoadingBar:loading",function (event, data) {
		 $scope.cargado = false;
		 return
	});

	$rootScope.$on ("cfpLoadingBar:completed",function (event, data) {
		$scope.cargado = true;
			return
	});

	$scope.imagenes = [];


	$scope.buscar = function(val, tipo, next){
		if(!tipo && next == undefined){
			var web = "/images?prefix="+val;
			$http.get(web, config).then( function(response){
					$scope.imagenes = response.data.results.resources;
			});
		}else if(tipo && next == undefined){
			var web = "/images?tag="+val;
			$http.get(web,config).then( function(response){
					$scope.imagenes = response.data.results.resources;
			});
		}else if(next == 'next'){
			var web = "/images?next="+$scope.next;
			$http.get(web, config).then( function(response){
				angular.forEach(response.data.results.resources, function(value, key){
						$scope.imagenes.push(value);
				});
					$scope.next = response.data.results.next_cursor;
			});
		}

	}

	$scope.close = function(result){
  	$uibModalInstance.close(result);
	};

	$scope.submit = function(result) {

		if (ban == 'banners')  {
			var response = "https://res.cloudinary.com/akolor/image/upload/c_lfill,g_auto,q_80,w_960/v"+ result.version + "/"+ result.public_id +".jpg";
		}else{
			var url_1080 = "https://res.cloudinary.com/akolor/image/upload/c_lfill,g_auto,h_700,q_80,w_1080/v"+ result.version + "/"+ result.public_id +".jpg";
			var url_720 = "https://res.cloudinary.com/akolor/image/upload/c_thumb,g_auto,h_407,q_80,w_720/v"+ result.version + "/"+ result.public_id +".jpg";
			var thumb_380 = "https://res.cloudinary.com/akolor/image/upload/c_thumb,h_407,q_80,w_720/v"+result.version+"/"+result.public_id+".jpg";
			var response = {
				url_raw : result.secure_url,
				url_1080 : url_1080,
				url_720 : url_720,
				thumb_380 : thumb_380
			};
		}



		$rootScope.$broadcast('imageReady', response);

		$uibModalInstance.close(response);
	};

	}]);

'use strict';

angular.module('articles').controller('imageUploaderController', ['$scope' , 'FileUploader', '$rootScope' , function($scope, FileUploader, $rootScope) {

        $scope.$watch('image_uploaded', function(){
            if($scope.image_uploaded == ''){
              $scope.image_uploaded = undefined;
            }
        });

        var uploader = $scope.uploader = new FileUploader({
            url: 'image',
            file: uploader,
            autoUpload: true
        });


        $rootScope.$on ("imageReady",function (event, data) {

          if (data.index == $scope.current || $scope.current
             == undefined) {
              $scope.image_uploaded = data.url;
          }

        });

        // FILTERS

        $scope.reset = function(){

            if($scope.tipo == 'banner'){
              var send = {
              url:undefined,
              index:$scope.current
              }
            }

            $rootScope.$broadcast('imageReady', send);
        };

        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        },
        {
              name: 'customFilter',
              fn: function(item /*{File|FileLikeObject}*/, options) {
                  return this.queue.length < 1;
              }
          });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {

        };
        uploader.onAfterAddingFile = function(fileItem) {

        };
        uploader.onAfterAddingAll = function(addedFileItems) {

        };
        uploader.onBeforeUploadItem = function(item) {

        };
        uploader.onProgressItem = function(fileItem, progress) {

        };
        uploader.onProgressAll = function(progress) {

        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {

        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {


        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {



          if($scope.tipo == 'banner'){

            response = {
              url:response.url_raw,
              index:$scope.current
            }
          }
          $rootScope.$broadcast('imageReady', response);
          $scope.image_uploaded = response;

        };
        uploader.onCompleteAll = function(fileItem, response, status, headers) {


        };


    }]);

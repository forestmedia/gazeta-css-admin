angular.module('core').directive('uploadFile', function() {

  return {
  	//RESTRICCION E para elemento y con A Atributo
    scope:{
      tipo:'@',
      current:'<',
      image_uploaded:'=file'
    },
  	restrict: "E",
    templateUrl: "templates/uploadFile-client-template.html",
    controller:'imageUploaderController'

  }
})
.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return parseInt(val, 10);
      });
      ngModel.$formatters.push(function(val) {
        return '' + val;
      });
    }
  };
});

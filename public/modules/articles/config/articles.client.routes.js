'use strict';

// Setting up route
angular.module('articles').config(['$stateProvider','$provide',
	function($stateProvider, $provide) {
		// Articles state routing
		$stateProvider.
		state('listArticles', {
			url: '/articles/list',
			templateUrl: 'modules/articles/views/list-articles.client.view.html'
		}).
		state('createArticle', {
			url: '/articles/create',
			templateUrl: 'modules/articles/views/create-article.client.view.html'
		}).
		state('viewArticle', {
			url: '/articles/:articleId/:articleDate',
			templateUrl: 'modules/articles/views/view-article.client.view.html'
		}).
		state('editConfig', {
			url: '/config',
			templateUrl: 'modules/articles/views/config.client.view.html',
			controller:'ConfigController'
		}).
		state('editArticle', {
			url: '/articles/:articleId/:articleDate/edit',
			templateUrl: 'modules/articles/views/edit-article.client.view.html'
		});
		// MODAL DE IMAGNES


			$provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$uibModal' ,function(taRegisterTool, taOptions, $uibModal){
	        // $delegate is the taOptions we are decorating
	        // register the tool with textAngular


					taRegisterTool('CustomInsertImage', {
						iconclass: "fa fa-picture-o",

						action: function() {
							var textAngular = this;
							var savedSelection = rangy.saveSelection();
							var modalInstance = $uibModal.open({
								// Put a link to your template here or whatever
								templateUrl: 'modules/articles/views/uploadModal.html',
								controller:"modalUploaderController"
							});

							modalInstance.result.then(function(imgUrl) {
								rangy.restoreSelection(savedSelection);
								textAngular.$editor().wrapSelection('insertHTML','<img src="'+imgUrl+'" class="adjust">');
							});
						}
					});
	        // add the button to the default toolbar definition
	        taOptions.toolbar[1].push('CustomInsertImage');
	        return taOptions;
	    }]);
			$provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions){
					// $delegate is the taOptions we are decorating
					// register the tool with textAngular
					taRegisterTool('colourRed', {
							iconclass: "fa fa-square red",
							action: function(){
									this.$editor().wrapSelection('forecolor', 'red');
							}
					});
					// add the button to the default toolbar definition
					taOptions.toolbar[1].push('colourRed');
					return taOptions;
			}]);


	}]);

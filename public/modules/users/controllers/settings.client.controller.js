'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication',
	function($scope, $http, $location, Users, Authentication) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

				$scope.sendfile = function(){
					$http.put('/settingsfile', $scope.lists).then( function(err, data){
							if(err){console.log(err);}else{
								$http.get('http://gazeta-ccs-files.s3-website-us-west-2.amazonaws.com/config.json').then(function(response, err){
										if(err){console.log(err);}else{
											console.log(response.data);
										 $scope.lists =	angular.fromJson(response.data);
										}
								});
							}
					});
				};

				$scope.iniciar = function(){

					$http.get('http://gazeta-ccs-files.s3-website-us-west-2.amazonaws.com/config.json').then(function(response, err){
							if(err){console.log(err);}else{
							 $scope.lists =	angular.fromJson(response.data);
							}
					});
				}

				$scope.tipo = {
					arrayElement:[]
				};

				//Widget Types
				$scope.typeWidget = [
					'category', 'tv', 'newsletter', 'twitter', 'stock', 'popular', 'likes' , 'hashtag', 'user' , 'custom', 'imageday', 'top', 'poll', 'banner'
				]

				//Count Options
				$scope.countOpt = [
				   1,3,5,7,9,11,13
				]

				//Side Options
				$scope.sideOpt = [
				   'Principal','Lateral'
				]

				//Section Type
				$scope.options = [
					{ label: 'Política', value: 0 },
					{ label: 'Economía', value: 1 },
					{ label: 'Sucesos', value: 2 },
					{ label: 'Opinión', value: 3 }
					];

					//Label Options
					$scope.boxOpt = [
					'Home',
					'Política',
					'Economía',
					'Sucesos',
					'Opinión'
						];

				//Push to array function
				$scope.pushArray = function(element){
					$scope.tipo.arrayElement.push(element);
					$scope.symbol = '';
				}

				//Add widget function
				$scope.addWidget = function(tipo, box){
						if(tipo.type == 'newsletter' || tipo.type == 'twitter' || tipo.type == 'popular' || tipo.type =='likes'){
							tipo.posicion = 'Lateral';
						}
						angular.forEach($scope.lists, function(element){
							if(element.label == box){
								element.widget.push(tipo);
							}
						});
						$scope.tipo = {
							arrayElement:[]
						};
				}

				$scope.removeItem = function(index, box){
					angular.forEach($scope.lists, function(element){
						if(element.label == box){
							element.widget.splice(index, 1);
						}
					});

				 }

				//List of labels
				$scope.lists = [
				{
						label: "Home",
						widget: []
				},
				{
						label: "Política",
						widget: []
				},
				{
						label: "Economía",
						widget: []
				},
				{
						label: "Sucesos",
						widget: []
				},
				{
						label: "Opinión",
						widget: []
				}
		];

		// Model to JSON for demo purpose
		$scope.$watch('lists', function(lists) {
				$scope.modelAsJson = angular.toJson(lists, true);
		}, true);


		//Section Options
		$scope.options = [
	    { label: 'Política', value: 0 , side: false},
	    { label: 'Economía', value: 1, side: false },
	    { label: 'Sucesos', value: 2, side: false },
	    { label: 'Opinión', value: 3, side: false }
	  	];


		// Check if there are additional accounts
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);

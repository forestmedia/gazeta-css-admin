'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'gccs';
	var applicationModuleVendorDependencies = ['ngResource', 'ngCookies',  'ngAnimate',  'ngTouch',  'ngSanitize',  'ui.router', 'templates-main' ,'ui.bootstrap','ui.bootstrap.typeahead','ui.bootstrap.modal' ,'ui.utils','decipher.tags','angularMoment','infinite-scroll','angular-loading-bar','ngDisqus','ga','angularFileUpload','dndLists','textAngular','ngAside','ngclipboard'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

// Setting HTML5 Location Mode

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('articles');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('banners');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('polls');

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Artículos', 'articles/list', '/articles(/create)?');
	}
]);

'use strict';

// Setting up route
angular.module('articles').config(['$stateProvider','$provide',
	function($stateProvider, $provide) {
		// Articles state routing
		$stateProvider.
		state('listArticles', {
			url: '/articles/list',
			templateUrl: 'modules/articles/views/list-articles.client.view.html'
		}).
		state('createArticle', {
			url: '/articles/create',
			templateUrl: 'modules/articles/views/create-article.client.view.html'
		}).
		state('viewArticle', {
			url: '/articles/:articleId/:articleDate',
			templateUrl: 'modules/articles/views/view-article.client.view.html'
		}).
		state('editConfig', {
			url: '/config',
			templateUrl: 'modules/articles/views/config.client.view.html',
			controller:'ConfigController'
		}).
		state('editArticle', {
			url: '/articles/:articleId/:articleDate/edit',
			templateUrl: 'modules/articles/views/edit-article.client.view.html'
		});
		// MODAL DE IMAGNES


			$provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$uibModal' ,function(taRegisterTool, taOptions, $uibModal){
	        // $delegate is the taOptions we are decorating
	        // register the tool with textAngular


					taRegisterTool('CustomInsertImage', {
						iconclass: "fa fa-picture-o",

						action: function() {
							var textAngular = this;
							var savedSelection = rangy.saveSelection();
							var modalInstance = $uibModal.open({
								// Put a link to your template here or whatever
								templateUrl: 'modules/articles/views/uploadModal.html',
								controller:"modalUploaderController"
							});

							modalInstance.result.then(function(imgUrl) {
								rangy.restoreSelection(savedSelection);
								textAngular.$editor().wrapSelection('insertHTML','<img src="'+imgUrl+'" class="adjust">');
							});
						}
					});
	        // add the button to the default toolbar definition
	        taOptions.toolbar[1].push('CustomInsertImage');
	        return taOptions;
	    }]);
			$provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions){
					// $delegate is the taOptions we are decorating
					// register the tool with textAngular
					taRegisterTool('colourRed', {
							iconclass: "fa fa-square red",
							action: function(){
									this.$editor().wrapSelection('forecolor', 'red');
							}
					});
					// add the button to the default toolbar definition
					taOptions.toolbar[1].push('colourRed');
					return taOptions;
			}]);


	}]);

'use strict';

angular.module('articles').controller('ArticlesController', ['$scope', '$stateParams', '$location','$rootScope', 'Authentication', 'Articles','$http' ,'Etiquetas','$uibModal','$timeout',
	function($scope, $stateParams, $location,$rootScope ,Authentication, Articles, $http , Etiquetas, $uibModal, $timeout) {
		$scope.authentication = Authentication;

		$scope.fecha = new Date();

		$scope.tooltipShow = function(inx){
			$scope.articles[inx].tooltipIsOpen = !$scope.articles[inx].tooltipIsOpen;
			$timeout(function () {
				$scope.articles[inx].tooltipIsOpen = !$scope.articles[inx].tooltipIsOpen;
			}, 2000);
		}



		$scope.$on('decipher.tags.removed', function(event, data) {

			if (!$scope.article) {
					delete data.tag;
			}

			else {
				if($scope.article.deleteTag != undefined){
						var tag = {
							label: data.tag.label,
							published_date: $scope.article.published_date
						}
						 $scope.article.deleteTag.push(tag);
						 console.log($scope.article.deleteTag);
				 }
			}


		});

		//MODAL
		$scope.galeria = function () {

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'modules/articles/views/image-gallery.client.view.html',
      controller: 'ArticlesGallery',
      size: 'lg'
    });

  };

		$scope.options = [
	    { label: 'Política', value: 0 },
	    { label: 'Economía', value: 1 },
	    { label: 'Seguridad', value: 2 },
	    { label: 'Opinión', value: 3 },
	    { label: 'Imagen del día', value: 4 }

	  	];


  	    $rootScope.$on ("cfpLoadingBar:loading",function (event, data) {
      		 $scope.cargado = false;
     			 return
     		});

		    $rootScope.$on ("cfpLoadingBar:completed",function (event, data) {
		     	$scope.cargado = true;
		        return
		    });

				$rootScope.$on ("imageReady",function (event, data) {
					$scope.images = data;
					if($scope.article){$scope.article.images = data;}

				});


	  	$scope.typeaheadOpts = {
			delimiter : ' ',
		  minLength: 3,
		  waitMs: 500,
		  allowsEditable: true
		};

		$scope.inicializar = function() {

		$scope.tags =  [{label:'gazeta'}, {label:'noticia'}];
		$scope.images  = { url_raw:"",url_1080:"",url_720:"", thumb_380:""};
		$scope.meta = {
			important:0,
			source:'GZCss'
		};
		$scope.status = 0;
		$scope.category = $scope.options[0];
		$scope.states = new Etiquetas();

		};

		var cuenta = 8;

		var params = {
			page: 1,
			count: 8

		};

		$scope.create = function() {
			var article = new Articles({
				title: this.title,
				headlines: this.headlines,
				content: this.content,
				category: this.category.value,
				tags: this.tags,
				images: this.images,
				meta: this.meta,
				status: this.status
			});
			article.$save(function(response) {
				$location.path('articles/' + response.slug + '/' + response.published_date);

				$scope.title = '';
				$scope.headlines = '';
				$scope.content = '';
				$scope.images = {};
				$scope.category = 0;
				$scope.tags = [];
				$scope.meta = {
					important:0,
					source:'GZCss'
				};
				$scope.status = 0;

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};


		$scope.remove = function(article) {
			if (article) {
				Articles.delete({
					articleId: article.slug,
					articleDate: article.published_date
				},function() {
					$location.path('articles/list');
				});

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove({
					articleId: $stateParams.articleId,
					articleDate: $stateParams.articleDate
				}, function() {
					$location.path('articles/list');
				});
			}
		};

		$scope.update = function() {

			var article = $scope.article;

			article.category = $scope.category.value;
			article.$update({
				articleId: $stateParams.articleId,
				articleDate: $stateParams.articleDate
			}, function() {
				$location.path('articles/' + article.slug + '/' + article.published_date);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.play = function(){

			$scope.verPic = !$scope.verPic;
		}




		$scope.find = function(cuenta) {


			params.count = cuenta;


			Articles.get(params, function(response){

					$scope.articles = response.results;

			});


		};

		$scope.findT = function(int) {
			var stat = int || 0;
			var url = "/articlesall?stat="+stat;
			$http.get(url).then(  function(response){
				$scope.articles = response.data;
			});
		};

		$scope.findOne = function() {

			$scope.states = new Etiquetas();
			$scope.article = Articles.get({
				articleId: $stateParams.articleId,
				articleDate: $stateParams.articleDate
			}, function(response){
				$scope.article.deleteTag = [];
				$rootScope.$broadcast("imageReady", response.images);
				$scope.category = $scope.options[response.category];
				if(!response.tags){$scope.article.tags = [{label:'gazeta'}, {label:'noticia'}];}

				});
		};
	}])

.controller('modalUploaderController', ["$scope", "FileUploader", "$uibModalInstance", function($scope, FileUploader, $uibModalInstance) {
					var uploader = $scope.uploader = new FileUploader({
							url: 'image',
							autoUpload: true
					});

					$scope.img = {
						url: ''
					};
					$scope.submit = function() {
						console.log($scope.img.url);
						$uibModalInstance.close($scope.img.url);
					};

					// FILTERS
					uploader.filters.push({
							name: 'imageFilter',
							fn: function(item /*{File|FileLikeObject}*/, options) {
									var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
									return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
							}
					},
					{
								name: 'customFilter',
								fn: function(item /*{File|FileLikeObject}*/, options) {
										return this.queue.length < 1;
								}
						});

					// CALLBACKS

					uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
							console.info('onWhenAddingFileFailed', item, filter, options);
					};
					uploader.onAfterAddingFile = function(fileItem) {
							console.info('onAfterAddingFile', fileItem);
					};
					uploader.onAfterAddingAll = function(addedFileItems) {
							console.info('onAfterAddingAll', addedFileItems);
					};
					uploader.onBeforeUploadItem = function(item) {
							console.info('onBeforeUploadItem', item);
					};
					uploader.onProgressItem = function(fileItem, progress) {
							console.info('onProgressItem', fileItem, progress);
					};
					uploader.onProgressAll = function(progress) {
							console.info('onProgressAll', progress);
					};
					uploader.onSuccessItem = function(fileItem, response, status, headers) {
							console.info('onSuccessItem', fileItem, response, status, headers);
					};
					uploader.onErrorItem = function(fileItem, response, status, headers) {
							console.info('onErrorItem', fileItem, response, status, headers);
					};
					uploader.onCancelItem = function(fileItem, response, status, headers) {

							console.info('onCancelItem', fileItem, response, status, headers);
					};
					uploader.onCompleteItem = function(fileItem, response, status, headers) {

						$scope.img.url = response.url_720;


						console.info('onCompleteItem', fileItem, response, status, headers);

					};
					uploader.onCompleteAll = function() {
							console.info('onCompleteAll');
					};

					console.info('uploader', uploader);
			}]);

// Controlador Galeria
	angular.module('articles').controller('ArticlesGallery', ['$scope', '$http','$uibModalInstance','$rootScope','$location',
		function($scope, $http, $uibModalInstance, $rootScope, $location) {

			var ban = $location.path().substring(1,8);

			if (ban == 'banners') {
					var url = "/images?tag=banner";
			}else{
						var url = "/images";
			}

			$http.get(url).then( function(response){

					$scope.imagenes = response.data.results.resources;
					$scope.tags = response.data.tags.tags;
					$scope.next =  response.data.results.next_cursor;
			});


	var config = {ignoreLoadingBar: true};

	$rootScope.$on ("cfpLoadingBar:loading",function (event, data) {
		 $scope.cargado = false;
		 return
	});

	$rootScope.$on ("cfpLoadingBar:completed",function (event, data) {
		$scope.cargado = true;
			return
	});

	$scope.imagenes = [];


	$scope.buscar = function(val, tipo, next){
		if(!tipo && next == undefined){
			var web = "/images?prefix="+val;
			$http.get(web, config).then( function(response){
					$scope.imagenes = response.data.results.resources;
			});
		}else if(tipo && next == undefined){
			var web = "/images?tag="+val;
			$http.get(web,config).then( function(response){
					$scope.imagenes = response.data.results.resources;
			});
		}else if(next == 'next'){
			var web = "/images?next="+$scope.next;
			$http.get(web, config).then( function(response){
				angular.forEach(response.data.results.resources, function(value, key){
						$scope.imagenes.push(value);
				});
					$scope.next = response.data.results.next_cursor;
			});
		}

	}

	$scope.close = function(result){
  	$uibModalInstance.close(result);
	};

	$scope.submit = function(result) {

		if (ban == 'banners')  {
			var response = "https://res.cloudinary.com/akolor/image/upload/c_lfill,g_auto,q_80,w_960/v"+ result.version + "/"+ result.public_id +".jpg";
		}else{
			var url_1080 = "https://res.cloudinary.com/akolor/image/upload/c_lfill,g_auto,h_700,q_80,w_1080/v"+ result.version + "/"+ result.public_id +".jpg";
			var url_720 = "https://res.cloudinary.com/akolor/image/upload/c_thumb,g_auto,h_407,q_80,w_720/v"+ result.version + "/"+ result.public_id +".jpg";
			var thumb_380 = "https://res.cloudinary.com/akolor/image/upload/c_thumb,h_407,q_80,w_720/v"+result.version+"/"+result.public_id+".jpg";
			var response = {
				url_raw : result.secure_url,
				url_1080 : url_1080,
				url_720 : url_720,
				thumb_380 : thumb_380
			};
		}



		$rootScope.$broadcast('imageReady', response);

		$uibModalInstance.close(response);
	};

	}]);

'use strict';

angular.module('articles').controller('imageUploaderController', ['$scope' , 'FileUploader', '$rootScope' , function($scope, FileUploader, $rootScope) {

        $scope.$watch('image_uploaded', function(){
            if($scope.image_uploaded == ''){
              $scope.image_uploaded = undefined;
            }
        });

        var uploader = $scope.uploader = new FileUploader({
            url: 'image',
            file: uploader,
            autoUpload: true
        });


        $rootScope.$on ("imageReady",function (event, data) {

          if (data.index == $scope.current || $scope.current
             == undefined) {
              $scope.image_uploaded = data.url;
          }

        });

        // FILTERS

        $scope.reset = function(){

            if($scope.tipo == 'banner'){
              var send = {
              url:undefined,
              index:$scope.current
              }
            }

            $rootScope.$broadcast('imageReady', send);
        };

        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        },
        {
              name: 'customFilter',
              fn: function(item /*{File|FileLikeObject}*/, options) {
                  return this.queue.length < 1;
              }
          });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {

        };
        uploader.onAfterAddingFile = function(fileItem) {

        };
        uploader.onAfterAddingAll = function(addedFileItems) {

        };
        uploader.onBeforeUploadItem = function(item) {

        };
        uploader.onProgressItem = function(fileItem, progress) {

        };
        uploader.onProgressAll = function(progress) {

        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {

        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {


        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {



          if($scope.tipo == 'banner'){

            response = {
              url:response.url_raw,
              index:$scope.current
            }
          }
          $rootScope.$broadcast('imageReady', response);
          $scope.image_uploaded = response;

        };
        uploader.onCompleteAll = function(fileItem, response, status, headers) {


        };


    }]);

'use strict';

angular.module('articles').controller('ConfigController', ['$scope', '$stateParams', '$location','$rootScope', 'Authentication', 'Articles','$http' ,'Etiquetas','$uibModal',
	function($scope, $stateParams, $location,$rootScope ,Authentication, Articles, $http , Etiquetas, $uibModal) {
		$scope.authentication = Authentication;

		$http.get('/config.json',function(res){
			console.log(res);
			$scope.conf = res.data;
		});


	}]);

angular.module('core').directive('uploadFile', function() {

  return {
  	//RESTRICCION E para elemento y con A Atributo
    scope:{
      tipo:'@',
      current:'<',
      image_uploaded:'=file'
    },
  	restrict: "E",
    templateUrl: "templates/uploadFile-client-template.html",
    controller:'imageUploaderController'

  }
})
.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return parseInt(val, 10);
      });
      ngModel.$formatters.push(function(val) {
        return '' + val;
      });
    }
  };
});

'use strict';

// Configuring the Articles module
angular.module('banners').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Banners', 'banners/list', '/banners(/create)?');
	}
]);

'use strict';

//Setting up route
angular.module('banners').config(['$stateProvider',
	function($stateProvider) {
		// Banners state routing
		$stateProvider.
		state('listBanners', {
			url: '/banners/list',
			templateUrl: 'modules/banners/views/list-banners.client.view.html'
		}).
		state('createBanner', {
			url: '/banners/create',
			templateUrl: 'modules/banners/views/create-banner.client.view.html'
		}).
		state('viewBanner', {
			url: '/banners/:bannerId',
			templateUrl: 'modules/banners/views/view-banner.client.view.html'
		}).
		state('editBanner', {
			url: '/banners/:bannerId/edit',
			templateUrl: 'modules/banners/views/edit-banner.client.view.html'
		});
	}
]);

'use strict';

// Banners controller
angular.module('banners').controller('BannersController', ['$scope', '$stateParams', '$location', 'Authentication', 'Banners','$uibModal', '$rootScope',
	function($scope, $stateParams, $location, Authentication, Banners, $uibModal, $rootScope) {
		$scope.authentication = Authentication;


		$scope.opPos = [
			{label:"Light", value:0},
			{label:"Standard", value:1},
			{label:"Platinum", value:2},
			{label:"Premium", value:3}
		];

		$scope.opPage = [
			{label:"300x250", width:300, height:250 , value:0},
			{label:"336x280", width:366, height:280 , value:1},
			{label:"728x90", width:728, height:90 , value:2},
			{label:"300x600", width:300, height:600 , value:3},
			{label:"320x100", width:320, height:100 , value:4}
		];

	var current  = Number;
		//MODAL
		$scope.galeria = function (indice) {

		current = indice;
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'modules/articles/views/image-gallery.client.view.html',
      controller: 'ArticlesGallery',
      size: 'lg'
    });
}


$scope.$on ("imageReady",function (event, data) {

		 if (data.index != undefined) {
			 $scope.banners[data.index].image.banner = data.url;
		 }else{
			 $scope.banners[current].image.banner = data;
		 }


});


		var removed = [];

		$scope.removeBanner = function(banner){

			for (var i in $scope.banners) {
				if ($scope.banners[i] === banner) {
					$scope.banners.splice(i, 1);
				}
			}

			var obj = {
				_id : banner._id
			};
			if(banner._id){removed.push(obj);console.log(removed);}

		}


		$scope.inicializar = function(){

			$scope.client = {};

			$scope.banners =[];
}

		$scope.newBanner = function(){
				  var nuevo = {
						image:{
							banner:""
						}
					};
						$scope.banners.push(nuevo);
		}

		// Create new Banner
		$scope.create = function() {
			// Create new Banner object
			$scope.banners.expires = new Date($scope.banners.expires);
			var banner = new Banners ({
				client: $scope.client,
				banners: $scope.banners
			});

			// Redirect after save
			banner.$save(function(response) {
				$location.path('banners/' + response._id +  "/edit");
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Banner
		$scope.remove = function(client) {
			console.log(client);
			if ( client ) {
				Banners.delete({
					bannerId: client._id
				},function() {
					$location.path('banners/list');
				});

				for (var i in $scope.clients) {
					if ($scope.clients [i] === client) {
						$scope.clients.splice(i, 1);
					}
				}
			} else {
				$scope.banner.$remove({
					bannerId: $stateParams.bannerId,
				}, function() {
					$location.path('banners');
				});
			}
		};

		// Update existing Banner
		$scope.update = function() {


			var todo = new Banners ({
				client: $scope.client,
				banners: $scope.banners,
				removed: removed
			});
			todo.$update(function(res) {
				$location.path('banners/' + res._id + "/edit");
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Banners
		$scope.find = function() {
			Banners.query( function(response){
				$scope.clients = response;
			});

		};

		// Find existing Banner
		$scope.findOne = function() {
			Banners.get({
				bannerId: $stateParams.bannerId
			}, function(response){

				response.banners[0].expires = new Date(response.banners[0].expires);
				$scope.client = response.client;
				$scope.banners = response.banners;

				});
		};
	}
]);

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider','cfpLoadingBarProvider',


	function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {

		cfpLoadingBarProvider.includeBar = true;

		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html',
			controller:'AuthenticationController'
		});
		$urlRouterProvider.otherwise('/');
	}
]);

angular.module('core').run(['amMoment','$rootScope', '$http' ,function(amMoment, $rootScope, $http) {
    amMoment.changeLocale('es');

		$rootScope.option = [
			  { label: 'Política', value: 0 },
			  { label: 'Economía', value: 1 },
			  { label: 'Sucesos', value: 2 },
			  { label: 'Opinión', value: 3 },
				{ label: 'Imagen', value: 4}
			];


}]);

angular.module('core').filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

angular.module('templates-main', ['modules/articles/views/config.client.view.html', 'modules/articles/views/create-article.client.view.html', 'modules/articles/views/edit-article.client.view.html', 'modules/articles/views/image-gallery.client.view.html', 'modules/articles/views/list-articles.client.view.html', 'modules/articles/views/uploadModal.html', 'modules/articles/views/view-article.client.view.html', 'modules/banners/views/create-banner.client.view.html', 'modules/banners/views/edit-banner.client.view.html', 'modules/banners/views/list-banners.client.view.html', 'modules/banners/views/view-banner.client.view.html', 'modules/core/views/article.client.view.html', 'modules/core/views/aside.html', 'modules/core/views/customTemplate.html', 'modules/core/views/header.client.view.html', 'modules/polls/views/create-poll.client.view.html', 'modules/polls/views/edit-poll.client.view.html', 'modules/polls/views/list-polls.client.view.html', 'modules/polls/views/view-poll.client.view.html', 'modules/users/views/authentication/signin.client.view.html', 'modules/users/views/authentication/signup.client.view.html', 'modules/users/views/password/forgot-password.client.view.html', 'modules/users/views/password/reset-password-invalid.client.view.html', 'modules/users/views/password/reset-password-success.client.view.html', 'modules/users/views/password/reset-password.client.view.html', 'modules/users/views/settings/change-password.client.view.html', 'modules/users/views/settings/edit-profile.client.view.html', 'modules/users/views/settings/home-settings.client.view.html', 'modules/users/views/settings/types.html', 'templates/box-client-template.html', 'templates/sidebar-client-template.html', 'templates/tags.html', 'templates/uploadFile-client-template.html', 'templates/widgets-client-template.html', 'templates/youtube-client-template.html']);

angular.module("modules/articles/views/config.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/articles/views/config.client.view.html",
    "<section><div class=row><div class=col-md-12><h3>HOME</h3><div class=\"col-md-12 centered\"><h4>Widget Categoria</h4><span ng-repeat=\"cat in conf.category\"></span></div></div></div></section>");
}]);

angular.module("modules/articles/views/create-article.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/articles/views/create-article.client.view.html",
    "<section data-ng-controller=ArticlesController data-ng-init=inicializar() ng-show=\"cargado && authentication.user\"><h1>Nuevo Artículo</h1><div class=col-md-12 no-sp><div class=\"col-xs-12 col-md-6\"><form name=articleForm class=form-horizontal data-ng-submit=create() novalidate><fieldset><div class=form-group ng-class=\"{ 'has-error': articleForm.title.$dirty && articleForm.title.$invalid }\"><label class=control-label for=title>Título</label><div class=controls><input name=title data-ng-model=title id=title class=form-control placeholder=Título required></div></div><div class=form-group><label class=control-label for=headlines>Encabezado</label><div class=controls><input name=headlines data-ng-model=headlines id=headlines class=form-control cols=30 rows=10 placeholder=Encabezado required></div></div><div class=form-group for=content><label class=control-label>Contenido:</label><text-angular ng-model=content ng-maxlength=8000></text-angular></div><a class=\"btn btn-success\" data-ng-click=galeria();>Galería <i class=\"glyphicon glyphicon-picture\"></i></a><upload-file></upload-file><div class=form-group><label class=control-label for=\"Image Des\">Descripción de Imagen</label><div class=controls><input name=imageDescription data-ng-model=images.description id=imageDescription class=form-control cols=30 rows=10 placeholder=\"Descripción de Imagen\" required></div><label class=control-label for=\"Image Des\">Autor de Imagen</label><div class=controls><input name=imageDescription data-ng-model=images.author id=imageDescription class=form-control cols=30 rows=10 placeholder=\"Autor de Imagen\" required></div></div><div class=form-group><label>Categoría<div><select ng-model=category ng-options=\"opt as opt.label for opt in options\"></select></div></label></div><div class=form-group><label class=control-label for=tags>Tags</label><div class=controls><tags typeahead-options=typeaheadOpts data-options=\"{addable: true, classes: {white: 'black', red: 'red'}}\" ng-src=\"s as s for s in states\" data-model=tags></tags></div></div><div class=checkbox><label>Fuente: <input name=meta-source data-ng-model=meta.source id=meta-source></label></div><div class=checkbox><label>Prioridad del Artículo<div><select ng-model=meta.important id=meta-important convert-to-number><option value=0>Nivel 0</option><option value=1>Nivel 1</option><option value=2>Nivel 2</option></select></div></label></div><br><div class=checkbox data-ng-show=\"authentication.user.roles[0]=='admin'\"><label>PUBLICAR NOTICIA (No seleccione si quiere guardar el borrador)<div><select ng-model=status convert-to-number><option value=0>Borrador</option><option value=1>Sin Revisar</option><option value=2>Visible</option><option value=3>Publicado</option></select></div></label></div><br><div class=form-group><input type=submit value=Save class=\"btn btn-success\"></div><div data-ng-show=error class=text-danger><strong data-ng-bind=error></strong></div></fieldset></form></div><div class=\"col-xs-12 col-md-6\"><h3>Preview</h3><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=\"dn-author ng-binding\">Autor: {{authentication.user.displayName}}</div></div><div class=clearfix></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><time data-ng-bind=\"created | date:'dd/MM/yyyy h:mma'\"></time></div><div class=dn-share><a class=sicon-tw href=\"http://twitter.com/share?text=Odamae%20-%20free%20contact%20form%20service%20from%20Phantomus&amp;url=http://dayandnight-demo.phantomus.com/odamae-free-contact-form-service-from-phantomus/\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=http://dayandnight-demo.phantomus.com/odamae-free-contact-form-service-from-phantomus/\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=http://dayandnight-demo.phantomus.com/odamae-free-contact-form-service-from-phantomus/\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><div><img lazy-src={{images.url_720}} class=\"titular-gz no-mg\"><h5 class=\"tag-gz-category no-mg\"><a>{{category.label}}</a></h5><h1 data-ng-bind=title></h1><h4 data-ng-bind=headlines></h4><div ta-bind ng-model=content></div><div class=\"col-xs-12 col-md-12 no-sp\"><h5 class=tag-gz-tags>Tags: <a class=\"label label-default tag-gz sp\" ng-repeat=\"tag in tags\">{{tag.label}}</a></h5></div></div></div></div></div></section>");
}]);

angular.module("modules/articles/views/edit-article.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/articles/views/edit-article.client.view.html",
    "<section data-ng-controller=ArticlesController data-ng-init=findOne()><h1>Editar Artículo</h1><div class=col-md-12><div class=\"col-xs-12 col-md-6\"><form name=articleForm class=form-horizontal data-ng-submit=update(articleForm.$valid) novalidate><fieldset><div class=form-group ng-class=\"{ 'has-error' : submitted && articleForm.title.$invalid}\"><label class=control-label for=title>Título</label><div class=controls><input name=title data-ng-model=article.title id=title class=form-control placeholder=Título required></div><div ng-show=\"submitted && articleForm.title.$invalid\" class=help-block><p ng-show=articleForm.title.$error.required class=text-danger>Título es requerido</p></div></div><div class=form-group ng-class=\"{ 'has-error' : submitted && articleForm.headlines.$invalid}\"><label class=control-label for=headlines>Encabezado</label><div class=controls><input name=headlines data-ng-model=article.headlines id=headlines class=form-control placeholder=Encabezado required></div><div ng-show=\"submitted && articleForm.headlines.$invalid\" class=help-block><p ng-show=articleForm.title.$error.required class=text-danger>Encabezado es requerido</p></div></div><div class=form-group ng-class=\"{ 'has-error' : submitted && articleForm.content.$invalid}\"><label class=control-label for=content>Contenido</label><text-angular ng-model=article.content ng-maxlength=8000></text-angular><div ng-show=\"submitted && articleForm.content.$invalid\" class=help-block><p ng-show=articleForm.content.$error.required class=text-danger>Contenido es requerido</p></div></div><br><a class=\"btn btn-success\" data-ng-click=galeria();><i class=\"glyphicon glyphicon-picture\"></i> Galería</a><upload-file></upload-file><div class=form-group><label class=control-label for=\"Image Des\">Descripción de Imagen</label><div class=controls><input name=imageDescription data-ng-model=article.images.description id=imageDescription class=form-control cols=30 rows=10 placeholder=\"Descripción de Imagen\" required></div><label class=control-label for=\"Image Des\">Autor de Imagen</label><div class=controls><input name=imageDescription data-ng-model=article.images.author id=imageAuthor class=form-control cols=30 rows=10 placeholder=\"Autor de Imagen\" required></div></div><div class=form-group><label>Categoría<div><select ng-model=category ng-options=\"opt as opt.label for opt in options\"></select></div></label></div><div class=form-group><label class=control-label for=tags>Tags</label><div class=controls><tags typeahead-options=typeaheadOpts data-options=\"{addable: true, classes: {white: 'black', red: 'red'}}\" ng-src=\"s as s for s in states\" data-model=article.tags name=tags id=tags></tags></div></div><div class=checkbox><label>Fuente: <input name=meta-source data-ng-model=article.meta.source id=meta-source></label></div><div class=checkbox><label>Prioridad del Artículo<div><select ng-model=article.meta.important id=meta-important convert-to-number><option value=0>Nivel 0</option><option value=1>Nivel 1</option><option value=2>Nivel 2</option></select></div></label></div><br><div class=checkbox data-ng-show=\"authentication.user.roles[0]=='admin'\"><label>PUBLICAR NOTICIA (No seleccione si quiere guardar el borrador)<div><select ng-model=article.status convert-to-number><option value=0>Borrador</option><option value=1>Sin Revisar</option><option value=2>Visible</option><option value=3>Publicado</option></select></div></label></div><br><div class=form-group><input type=submit value=Update class=\"btn btn-primary\"> <a class=\"btn btn-danger\" data-ng-show=\"(authentication.user._id == article.user._id) || (authentication.user.roles[0]=='admin')\" data-ng-click=remove();><i class=\"glyphicon glyphicon-trash\"></i></a></div><div data-ng-show=error class=text-danger><strong data-ng-bind=error></strong></div></fieldset></form></div><div class=\"col-xs-12 col-md-6\"><h3>Preview</h3><box article=article tipo=3></box></div></div></section>");
}]);

angular.module("modules/articles/views/image-gallery.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/articles/views/image-gallery.client.view.html",
    "<div class=modal-header><button type=button class=close data-dismiss=modal aria-label=Close ng-click=close()><span aria-hidden=true>&times;</span></button><h4 class=modal-title>Galley Cloud</h4></div><div class=modal-body><input ng-model=query> <button type=button ng-click=buscar(query)>buscar</button><h2>Tags</h2><div class=row><span ng-repeat=\"tag in tags\"><a ng-click=buscar(tag,true) href=\"\">{{tag}}</a></span></div><h2>Images</h2><div class=row><div class=col-md-3 ng-repeat=\"image in imagenes\"><img ng-click=submit(image) class=col-md-12 ng-src=https://res.cloudinary.com/akolor/image/upload/c_thumb,h_407,q_80,w_720/v{{image.version}}/{{image.public_id}}.jpg alt=\"{{image.public_id}}\"><h5>{{image.public_id}}</h5><span ng-repeat=\"tag in image.tags\">{{tag}}</span></div></div></div><div class=modal-footer><button type=button class=\"btn btn-default\" ng-click=close()>Close</button> <button type=button class=\"btn btn-primary\" ng-click=\"buscar('',false,'next')\">More</button></div>");
}]);

angular.module("modules/articles/views/list-articles.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/articles/views/list-articles.client.view.html",
    "<section data-ng-controller=ArticlesController data-ng-init=findT()><div class=page-header><h1>Artículos</h1><nav aria-label=...><ul class=pager><li class=previous><a href=/#!/articles/create>Nuevo Artículo</a></li></ul></nav></div><div class=list-group><uib-tabset active=activeJustified justified=true><uib-tab index=0 heading=Borrador ng-click=findT(0)></uib-tab><uib-tab index=1 heading=Revisar ng-click=findT(1)></uib-tab><uib-tab index=2 heading=Visible ng-click=findT(2)></uib-tab><uib-tab index=3 heading=Publicado ng-click=findT(3)></uib-tab></uib-tabset><div data-ng-repeat=\"article in articles\" data-ng-href=#!/articles/{{article.slug}}/{{article.published_date}} class=list-group-item ng-show=cargado><a data-ng-href=#!/articles/{{article.slug}}/{{article.published_date}}><small class=list-group-item-text>Posted on <span data-ng-bind=\"article.published_date | date:'mediumDate'\"></span> by <span data-ng-bind=article.user.displayName></span> date <span am-=\"article.published_date \"></span></small></a> <a class=\"btn btn-primary pull-right\" href=/#!/articles/{{article.slug}}/{{article.published_date}}/edit data-ng-show=\"(authentication.user.id == article.user.id) || (authentication.user.roles[0]=='admin')\"><i class=\"glyphicon glyphicon-edit\"></i></a> <a class=\"btn btn-warning pull-right\" ng-click=remove(article) data-ng-show=\"(authentication.user.id == article.user.id) || (authentication.user.roles[0]=='admin')\"><i class=\"glyphicon glyphicon-trash\"></i></a><div class=row><div class=col-md-7><a class=\"btn btn-primary pull-right\" ng-click=tooltipShow($index) ngclipboard data-clipboard-text={{article._id}}><i class=\"glyphicon glyphicon-copy\"></i></a> <a data-ng-href=#!/articles/{{article.slug}}/{{article.published_date}}><h4 class=list-group-item-heading data-ng-bind=article._id></h4><div uib-tooltip=Copied! tooltip-is-open=article.tooltipIsOpen tooltip-placement=bottom></div><h4 class=list-group-item-heading data-ng-bind=article.title></h4><h5 class=list-group-item-heading data-ng-bind=article.headlines></h5><p class=list-group-item-text ng-bind-html=\"article.content | limitTo:30\"></p><p class=list-group-item-text>Categoría: {{options[article.category].label}}</p><p class=list-group-item-text>Artículo Importante: {{article.meta.important}}</p><p class=list-group-item-text data-ng-repeat=\"tags in article.tags\">#{{tags.label}}</p></a></div><div class=col-md-4><img class=\"col-xs-12 col-md-12\" lazy-src={{article.images.thumb_380}} alt=\"\"></div></div></div></div><div class=\"alert alert-warning text-center\" data-ng-if=\"articles.$resolved && !articles.length\">No articles yet, why don't you <a href=/#!/articles/create>create one</a>?</div></section>");
}]);

angular.module("modules/articles/views/uploadModal.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/articles/views/uploadModal.html",
    "<div class=row nv-file-drop=\"\" uploader=uploader style=padding:10px><div><div class=col-md-3 ng-show=!img.url><h3>Select file</h3><div ng-show=uploader.isHTML5><div class=\"well my-drop-zone\" nv-file-over=\"\" uploader=uploader>Drop File Here</div></div><input type=file nv-file-select=\"\" uploader=\"uploader\"><br></div><div class=col-md-9 style=\"margin-bottom: 40px\"><h2>Uploads Image</h2><table class=table><thead><tr><th width=50%>Name</th><th ng-show=uploader.isHTML5>Size</th><th ng-show=uploader.isHTML5>Progress</th></tr></thead><tbody><tr ng-repeat=\"item in uploader.queue\"><td><strong>{{ item.file.name }}</strong><div ng-show=uploader.isHTML5 ng-thumb=\"{ file: item._file, height: 100 }\"></div></td><td ng-show=uploader.isHTML5 nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td><td ng-show=uploader.isHTML5><div class=progress style=\"margin-bottom: 0\"><div class=progress-bar role=progressbar ng-style=\"{ 'width': item.progress + '%' }\"></div></div></td></tr></tbody></table><input ng-model=img.url ng-show=img.url><button ng-click=submit() ng-show=img.url>OK</button></div></div></div>");
}]);

angular.module("modules/articles/views/view-article.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/articles/views/view-article.client.view.html",
    "<section data-ng-controller=ArticlesController data-ng-init=findOne() ng-show=cargado><div class=row><div class=pull-right data-ng-show=\"(authentication.user._id == article.user._id) || (authentication.user.roles[0]=='admin')\"><a class=\"btn btn-primary\" ng-href=/#!/articles/{{article.slug}}/{{article.published_date}}/edit><i class=\"glyphicon glyphicon-edit\"></i></a> <a class=\"btn btn-danger\" data-ng-click=remove();><i class=\"glyphicon glyphicon-trash\"></i></a></div><div class=\"col-xs-12 col-md-11 centering\"><div class=\"col-xs-12 col-md-8\"><box article=article tipo=3></box></div><div class=\"col-xs-12 col-sm-12 col-md-4 no-sp\"><div class=\"col-xs-12 col-sm-12 col-md-12 dn-read-gz\"><h4 class=\"col-xs-12 col-sm-12 col-md-12 bd-bt\">ARTÍCULOS RELACIONADOS</h4><box article=article tipo=2></box><box article=article tipo=2></box><box article=article tipo=2></box><box article=article tipo=2></box></div><newsletter></newsletter><mostread></mostread></div></div></div></section>");
}]);

angular.module("modules/banners/views/create-banner.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/banners/views/create-banner.client.view.html",
    "<section data-ng-controller=BannersController ng-init=inicializar()><div class=page-header><h1>Nuevo Cliente</h1></div><div class=col-md-12><form class=form-horizontal data-ng-submit=create() novalidate><fieldset><h2>Información Cliente</h2><div class=form-group><label class=control-label for=name>Nombre Cliente</label><div class=controls><input data-ng-model=client.name id=client-name class=form-control placeholder=\"Nombre Cliente\" required></div></div><div class=form-group><label class=control-label for=name>Contacto</label><div class=controls><input data-ng-model=client.info.contact id=client-contact class=form-control placeholder=Contacto></div></div><div class=form-group><label class=control-label for=name>Email</label><div class=controls><input type=email data-ng-model=client.info.email id=client-email class=form-control placeholder=Email></div></div><div class=form-group><label class=control-label for=name>Teléfono</label><div class=controls><input data-ng-model=client.info.phone id=client-contact-phone class=form-control placeholder=Teléfono></div></div><div class=form-group><label class=control-label for=name>Dirección</label><div class=controls><input data-ng-model=client.info.address id=client-address class=form-control placeholder=Dirección></div></div><div class=form-group><label class=control-label for=name>Plan</label><div><select ng-model=client.plan><option value=0>Light</option><option value=1>Standard</option><option value=2>Platinum</option><option value=3>Premium</option></select></div></div><h2>Banners</h2><nav aria-label=...><ul class=pager><li class=previous><a ng-click=newBanner()>Agregar Banner</a></li></ul></nav><div class=\"borCorner col-md-12\" style=margin-top:15px ng-repeat=\"banner in banners\"><div class=pull-right><span class=\"glyphicon glyphicon-remove-circle\" aria-hidden=true ng-click=removeBanner(banner)></span></div><div class=form-group><label class=control-label for=name>Nombre</label><div class=controls><input data-ng-model=banner.name id=name class=form-control placeholder=Nombre required></div></div><div class=form-group><label class=control-label for=name>Fecha</label><div class=controls><input type=date data-ng-model=banner.expires id=name class=form-control required></div></div><div class=form-group><label class=control-label for=image-type>Tipo</label><div><select ng-model=banner.image.type><option value=Code>Código</option><option value=Link>Link</option></select></div><label class=control-label for=format>Formato</label><div><select ng-model=banner.format convert-to-number><option value=0>300x250</option><option value=1>336x280</option><option value=2>728x90</option><option value=3>300x600</option><option value=4>320x100</option></select></div><label class=control-label for=image>Status</label><div class=checkbox><select ng-model=banner.status convert-to-number><option value=0>Offline</option><option value=1>Online</option></select></div><upload-file tipo=banner current=$index></upload-file><img lazy-src={{banner.image.banner}} class=img-responsive><div><a class=\"btn btn-success\" style=margin-top:15px data-ng-click=galeria($index);>Galería <i class=\"glyphicon glyphicon-picture\"></i></a></div></div></div><div class=form-group><input type=submit class=\"btn btn-default\" style=\"margin-top:15px; margin-left:20px\"></div><div data-ng-show=error class=text-danger><strong data-ng-bind=error></strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/banners/views/edit-banner.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/banners/views/edit-banner.client.view.html",
    "<section data-ng-controller=BannersController ng-init=findOne()><div class=page-header><h1>Edit Banner</h1></div><div class=col-md-12><form class=form-horizontal data-ng-submit=update() novalidate><fieldset><h2>Información Cliente</h2><div class=form-group><label class=control-label for=name>Nombre Cliente</label><div class=controls><input data-ng-model=client.name id=client-name class=form-control placeholder=\"Nombre Cliente\" required></div></div><div class=form-group><label class=control-label for=name>Contacto</label><div class=controls><input data-ng-model=client.info.contact id=client-contact class=form-control placeholder=Contacto></div></div><div class=form-group><label class=control-label for=name>Email</label><div class=controls><input type=email data-ng-model=client.info.email id=client-email class=form-control placeholder=Email></div></div><div class=form-group><label class=control-label for=name>Teléfono</label><div class=controls><input data-ng-model=client.info.phone id=client-contact-phone class=form-control placeholder=Teléfono></div></div><div class=form-group><label class=control-label for=name>Dirección</label><div class=controls><input data-ng-model=client.info.address id=client-address class=form-control placeholder=Dirección></div></div><div class=form-group><label class=control-label for=name>Plan</label><div><select ng-model=client.plan><option value=0>Light</option><option value=1>Standard</option><option value=2>Platinum</option><option value=3>Premium</option></select></div></div><h2>Banners</h2><nav aria-label=...><ul class=pager><li class=previous><a ng-click=newBanner()>Agregar Banner</a></li></ul></nav><div class=\"borCorner col-md-12\" style=margin-top:15px ng-repeat=\"banner in banners\"><div class=pull-right><span class=\"glyphicon glyphicon-remove-circle\" aria-hidden=true ng-click=removeBanner(banner)></span></div><div class=form-group><label class=control-label for=name>Nombre</label><div class=controls><input data-ng-model=banner.name id=name class=form-control placeholder=Nombre required></div></div><div class=form-group><label class=control-label for=name>Fecha</label><div class=controls><input type=date data-ng-model=banner.expires id=name class=form-control placeholder=Fecha required></div></div><div class=form-group><label class=control-label for=image-type>Tipo</label><div><select ng-model=banner.image.type><option value=Code>Código</option><option value=Link>Link</option></select></div><label class=control-label for=format>Formato</label><div><select ng-model=banner.format><option value=0>300x250</option><option value=1>336x280</option><option value=2>728x90</option><option value=3>300x600</option><option value=4>320x100</option></select></div><label class=control-label for=image>Status</label><div class=checkbox><select ng-model=banner.status convert-to-number><option value=0>Offline</option><option value=1>Online</option></select></div><upload-file tipo=banner current=$index file=banner.image.banner></upload-file><img lazy-src={{banner.image.banner}} class=img-responsive><div><a class=\"btn btn-success\" style=margin-top:15px data-ng-click=galeria($index);>Galería <i class=\"glyphicon glyphicon-picture\"></i></a></div></div></div><div class=form-group><input type=submit class=\"btn btn-default\" style=\"margin-top:15px; margin-left:20px\"></div><div data-ng-show=error class=text-danger><strong data-ng-bind=error></strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/banners/views/list-banners.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/banners/views/list-banners.client.view.html",
    "<section data-ng-controller=BannersController data-ng-init=find()><div class=page-header><h1>Banners</h1><nav aria-label=...><ul class=pager><li class=previous><a href=/#!/banners/create>Nuevo Banner</a></li></ul></nav></div><div data-ng-repeat=\"client in clients\" data-ng-href=#!/banners/{{client._id}} class=list-group-item><a class=\"btn btn-primary pull-right\" href=/#!/banners/{{client._id}}/edit data-ng-show=(authentication.user)><i class=\"glyphicon glyphicon-edit\"></i></a> <a class=\"btn btn-warning pull-right\" ng-click=remove(client) data-ng-show=\"(authentication.user.roles[0]=='admin')\"><i class=\"glyphicon glyphicon-trash\"></i></a><h4 class=list-group-item-heading>Nombre: {{client.name}}</h4></div><div class=\"alert alert-warning text-center\" data-ng-hide=\"!clients.$resolved || clients.length\">No Banners yet, why don't you <a href=/#!/banners/create>create one</a>?</div></section>");
}]);

angular.module("modules/banners/views/view-banner.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/banners/views/view-banner.client.view.html",
    "<section data-ng-controller=BannersController data-ng-init=findOne()><div class=page-header><h1 data-ng-bind=client.name></h1></div><div class=pull-right data-ng-show=\"((authentication.user) && (authentication.user._id == banner.user._id))\"><a class=\"btn btn-primary\" href=/#!/banners/{{client._id}}/edit><i class=\"glyphicon glyphicon-edit\"></i></a><div class=clo-md-12 ng-repeat=\"banner in banners\"><p data-ng-bind=banner.name></p><img lazy-src=\"{{banner.image.banner}}\"></div><a class=\"btn btn-primary\" data-ng-click=remove();><i class=\"glyphicon glyphicon-trash\"></i></a></div><small></small></section>");
}]);

angular.module("modules/core/views/article.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/article.client.view.html",
    "<section data-ng-init=findOne() ng-show=cargado><div class=\"col-xs-12 col-md-9 no-sp centering\"><div class=\"dn-line clearfix\"><box article=article tipo=3 size=700></box><div class=\"col-md-12 mg-top no-sp\"><banner banners=0></banner></div></div><h3 class=\"col-xs-12 col-md-12\">Artículos Relacionados</h3><div class=\"col-xs-12 col-md-12 no-sp dn-line clearfix\"><div class=\"col-xs-12 col-md-6\"><box article=article tipo=2></box></div><div class=\"col-xs-12 col-md-6\"><box article=article tipo=2></box></div><div class=\"col-xs-12 col-md-6\"><box article=article tipo=2></box></div><div class=\"col-xs-12 col-md-6\"><box article=article tipo=2></box></div></div></div><div class=\"col-xs-12 col-md-8 centering\"><h3 class=\"col-xs-12 col-md-12 no-sp\">Comentarios</h3><dir-disqus config=disqusConfig></dir-disqus></div></section>");
}]);

angular.module("modules/core/views/aside.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/aside.html",
    "<div class=modal-header data-ng-controller=AuthenticationController><h3 class=modal-title>{{authentication.user.displayName}}</h3></div><div class=modal-body><ul class=\"nav navbar-nav navbar-left menuli\"><li><a href=/#!/articles/list role=button aria-expanded=false ng-click=ok($event)>Artículos</a></li><li><a href=/#!/banners/list role=button aria-expanded=false ng-click=ok($event)>Banners</a></li><li><a href=/#!/polls/list role=button aria-expanded=false ng-click=ok($event)>Encuestas</a></li><li><a href=/#!/settings/home>Editar Home</a></li><li><a href=/#!/settings/profile>Editar Perfil</a></li><li><a href=/#!/settings/password>Cambiar Password</a></li><li><a href=/auth/signout>Salir</a></li></ul></div>");
}]);

angular.module("modules/core/views/customTemplate.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/customTemplate.html",
    "<a href=/#!/articulo/{{match.model.slug}}/{{match.model.published_date}} ng-controller=HeaderController ng-click=changeSearch()><span bind-html-unsafe=\"match.model.title | uibTypeaheadHighlight:query\">{{match.model.title}}</span> , fecha:{{match.model.created | date:'dd-MM-yyyy h:mm a'}}</a>");
}]);

angular.module("modules/core/views/header.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/core/views/header.client.view.html",
    "<div class=container data-ng-controller=HeaderController><div class=navbar-header><button class=\"navbar-toggle toggle-menu menu-right push-body\" type=button ng-click=\"openAside('left', true)\" ng-show=authentication.user><span class=sr-only></span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button> <a href=\"/#!/\" class=navbar-brand><img lazy-src=modules/core/img/gazeta-svg/gazeta-ccs-oscuro.svg></a></div><nav class=\"navbar-collapse collapse navbar-left\" collapse=!isCollapsed role=navigation><ul class=\"nav navbar-nav navbar-right\" data-ng-show=authentication.user><li class=dropdown uib-dropdown><a class=dropdown-toggle uib-dropdown-toggle><span data-ng-bind=authentication.user.displayName></span> <b class=caret></b></a><ul class=dropdown-menu uib-dropdown-menu><li data-ng-show=\"authentication.user.roles == 'admin'\"><a href=/#!/settings/home>Editar HOME</a></li><li><a href=/#!/settings/profile>Editar perfil</a></li><li data-ng-show=\"authentication.user.provider === 'local'\"><a href=/#!/settings/password>Cambiar password</a></li><li class=divider></li><li><a href=/auth/signout>Salir</a></li></ul></li></ul><ul class=\"nav navbar-nav navbar-right\" data-ng-if=menu.shouldRender(authentication.user);><li data-ng-repeat=\"item in menu.items | orderBy: 'position'\" data-ng-if=item.shouldRender(authentication.user); ng-switch=item.menuItemType ui-route={{item.uiRoute}} class={{item.menuItemClass}} ng-class=\"{active: ($uiRoute)}\"><a href=/#!/{{item.link}}><span data-ng-bind=item.title></span></a></li></ul></nav><ul class=\"nav navbar-nav navbar-collapse collapse navbar-right\"><li><button class=\"navbar-toggle toggle-menu menu-right push-body inh\" type=button ng-click=\"openAside('left', true)\" ng-show=authentication.user><span class=sr-only></span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button></li></ul></div>");
}]);

angular.module("modules/polls/views/create-poll.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/polls/views/create-poll.client.view.html",
    "<section data-ng-controller=PollsController ng-init=inicializar()><div class=page-header><h1>Nueva Encuesta</h1></div><div class=col-md-12><form class=form-horizontal data-ng-submit=create() novalidate><fieldset><div class=form-group><label class=control-label for=question>Pregunta</label><div class=controls><input data-ng-model=question id=question class=form-control placeholder=Pregunta required></div></div><div class=form-group><label class=control-label for=name>Opciones</label><div class=controls><input data-ng-model=dataOpt class=form-control placeholder=Opciones required><div><button type=button ng-click=addOpt() style=margin-top:15px>Add</button></div><div style=margin-top:15px ng-repeat=\"opc in options\">{{opc.text}} <span class=\"glyphicon glyphicon-remove-circle\" ng-click=removeOpt($index)></span></div></div></div><div class=form-group><input type=submit class=\"btn btn-default\"></div><div data-ng-show=error class=text-danger><strong data-ng-bind=error></strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/polls/views/edit-poll.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/polls/views/edit-poll.client.view.html",
    "<section data-ng-controller=PollsController data-ng-init=findOne()><div class=page-header><h1>Editar encuesta</h1></div><div class=col-md-12><form class=form-horizontal data-ng-submit=update() novalidate><fieldset><div class=form-group><label class=control-label for=title>Pregunta</label><div class=controls><input data-ng-model=poll.question id=question class=form-control placeholder=Pregunta required></div></div><div class=form-group><label class=control-label for=name>Opciones</label><div class=controls><input data-ng-model=dataOpt class=form-control placeholder=Opciones required><div><button type=button ng-click=addOpt() style=margin-top:15px>Add</button></div><div style=margin-top:15px ng-repeat=\"opc in options\">{{opc.text}}, {{opc.vote}} votos. <span class=\"glyphicon glyphicon-remove-circle\" ng-click=removeOpt($index)></span></div></div></div><div class=form-group><input type=submit value=Update class=\"btn btn-default\"></div><div data-ng-show=error class=text-danger><strong data-ng-bind=error></strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/polls/views/list-polls.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/polls/views/list-polls.client.view.html",
    "<section data-ng-controller=PollsController data-ng-init=find()><div class=page-header><h1>Encuestas</h1><nav aria-label=...><ul class=pager><li class=previous><a href=/#!/polls/create>Nueva Encuesta</a></li></ul></nav></div><div class=list-group><div data-ng-repeat=\"poll in polls track by $index\" data-ng-href=#!/polls/{{poll._id}} class=list-group-item><a class=\"btn btn-primary pull-right\" href=/#!/polls/{{poll._id}}/edit data-ng-show=\"(authentication.user.id == article.user.id) || (authentication.user.roles[0]=='admin')\"><i class=\"glyphicon glyphicon-edit\"></i></a> <a class=\"btn btn-warning pull-right\" ng-click=remove(poll) data-ng-show=\"(authentication.user.id == poll.user.id) || (authentication.user.roles[0]=='admin')\"><i class=\"glyphicon glyphicon-trash\"></i></a> <small class=list-group-item-text>Posted on <span data-ng-bind=\"poll.created_date | date:'medium'\"></span> by <span data-ng-bind=poll.user.displayName></span></small><h4 class=list-group-item-heading data-ng-bind=poll.question></h4><ul><li ng-repeat=\"opt in poll.options\">{{opt.text}}</li></ul></div></div><div class=\"alert alert-warning text-center\" data-ng-hide=\"!polls.$resolved || polls.length\">No Polls yet, why don't you <a href=/#!/polls/create>create one</a>?</div></section>");
}]);

angular.module("modules/polls/views/view-poll.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/polls/views/view-poll.client.view.html",
    "<section data-ng-controller=PollsController data-ng-init=findOne()><div class=page-header><h1 data-ng-bind=poll.name></h1></div><div class=pull-right data-ng-show=\"((authentication.user) && (authentication.user._id == poll.user._id))\"><a class=\"btn btn-primary\" href=/#!/polls/{{poll._id}}/edit><i class=\"glyphicon glyphicon-edit\"></i></a> <a class=\"btn btn-primary\" data-ng-click=remove();><i class=\"glyphicon glyphicon-trash\"></i></a></div><small><em class=text-muted>Posted on <span data-ng-bind=\"poll.created | date:'mediumDate'\"></span> by <span data-ng-bind=poll.user.displayName></span></em></small><h2>{{poll.question}}</h2><p ng-repeat=\"res in poll.choices\">{{res.text}}</p></section>");
}]);

angular.module("modules/users/views/authentication/signin.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/authentication/signin.client.view.html",
    "<section class=row data-ng-controller=AuthenticationController ng-show=!authentication.user><h3 class=\"col-md-12 text-center\">Iniciar Sesion</h3><div class=\"col-xs-offset-2 col-xs-8 col-md-offset-5 col-md-2\"><form data-ng-submit=signin() class=\"signin form-horizontal\" autocomplete=off><fieldset><div class=form-group><label for=username>Username</label><input id=username name=username class=form-control data-ng-model=credentials.username placeholder=Username></div><div class=form-group><label for=password>Password</label><input type=password id=password name=password class=form-control data-ng-model=credentials.password placeholder=Password></div><div class=\"text-center form-group\"><button type=submit class=\"btn btn-primary\">Sign in</button></div><div class=forgot-password><a href=/#!/password/forgot>Forgot your password?</a></div><div data-ng-show=error class=\"text-center text-danger\"><strong data-ng-bind=error></strong></div></fieldset></form></div></section><section class=row data-ng-controller=AuthenticationController ng-show=authentication.user><h3 class=\"col-md-12 text-center\">Panel de Control</h3><div class=\"col-xs-offset-2 col-xs-8 col-md-offset-5 col-md-2\">{{authentication.user.username}}</div></section>");
}]);

angular.module("modules/users/views/authentication/signup.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/authentication/signup.client.view.html",
    "<section class=row ng-show=!authentication.user><h3 class=\"col-md-12 text-center\">REGISTRO</h3><div class=\"col-xs-offset-2 col-xs-8 col-md-offset-5 col-md-2\"><form name=userForm data-ng-submit=signup() class=\"signin form-horizontal\" novalidate autocomplete=off><fieldset><div class=form-group><label for=firstName>First Name</label><input required id=firstName name=firstName class=form-control data-ng-model=credentials.firstName placeholder=\"First Name\"></div><div class=form-group><label for=lastName>Last Name</label><input id=lastName name=lastName class=form-control data-ng-model=credentials.lastName placeholder=\"Last Name\"></div><div class=form-group><label for=email>Email</label><input type=email id=email name=email class=form-control data-ng-model=credentials.email placeholder=Email></div><div class=form-group><label for=username>Username</label><input id=username name=username class=form-control data-ng-model=credentials.username placeholder=Username></div><div class=form-group><label for=image>Image</label><input id=image name=image class=form-control data-ng-model=credentials.image placeholder=Image></div><div class=form-group><label for=image>Description</label><input id=description name=description class=form-control data-ng-model=credentials.meta.description placeholder=Description></div><div class=form-group><h3>Social Media</h3><label for=meta>Facebook user</label><input id=facebook name=facebook class=form-control data-ng-model=credentials.meta.facebook placeholder=\"Facebook User\"><label for=image>Twitter user</label><input id=twitter name=twitter class=form-control data-ng-model=credentials.meta.twitter placeholder=\"Twitter User\"><label for=image>Instagram user</label><input id=instagram name=instagram class=form-control data-ng-model=credentials.meta.instagram placeholder=\"Instagram User\"><label for=image>Website</label><input id=website name=website class=form-control data-ng-model=credentials.meta.website placeholder=Website></div><div class=form-group><label>Roles</label><select ng-model=credentials.roles><option value=user>User</option><option value=admin>Admin</option><option value=editor>Editor</option></select></div><div class=form-group><label for=level>Roles</label><input id=roles name=roles class=form-control data-ng-model=credentials.roles placeholder=Roles readonly></div><div class=form-group><label for=password>Password</label><input type=password id=password name=password class=form-control data-ng-model=credentials.password placeholder=Password></div><div class=\"text-center form-group\"><button type=submit class=\"btn btn-large btn-primary\">Sign up</button>&nbsp; or&nbsp; <a href=/#!/signin class=show-signup>Sign in</a></div><div data-ng-show=error class=\"text-center text-danger\"><strong data-ng-bind=error></strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/users/views/password/forgot-password.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/password/forgot-password.client.view.html",
    "<section class=row data-ng-controller=PasswordController><h3 class=\"col-md-12 text-center\">Restore your password</h3><p class=\"small text-center\">Enter your account username.</p><div class=\"col-xs-offset-2 col-xs-8 col-md-offset-5 col-md-2\"><form data-ng-submit=askForPasswordReset() class=\"signin form-horizontal\" autocomplete=off><fieldset><div class=form-group><input id=username name=username class=form-control data-ng-model=credentials.username placeholder=Username></div><div class=\"text-center form-group\"><button type=submit class=\"btn btn-primary\">Submit</button></div><div data-ng-show=error class=\"text-center text-danger\"><strong>{{error}}</strong></div><div data-ng-show=success class=\"text-center text-success\"><strong>{{success}}</strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/users/views/password/reset-password-invalid.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/password/reset-password-invalid.client.view.html",
    "<section class=\"row text-center\"><h3 class=col-md-12>Password reset is invalid</h3><a href=/#!/password/forgot class=col-md-12>Ask for a new password reset</a></section>");
}]);

angular.module("modules/users/views/password/reset-password-success.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/password/reset-password-success.client.view.html",
    "<section class=\"row text-center\"><h3 class=col-md-12>Password successfully reset</h3><a href=\"/#!/\" class=col-md-12>Continue to home page</a></section>");
}]);

angular.module("modules/users/views/password/reset-password.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/password/reset-password.client.view.html",
    "<section class=row data-ng-controller=PasswordController><h3 class=\"col-md-12 text-center\">Reset your password</h3><div class=\"col-xs-offset-2 col-xs-8 col-md-offset-5 col-md-2\"><form data-ng-submit=resetUserPassword() class=\"signin form-horizontal\" autocomplete=off><fieldset><div class=form-group><label for=newPassword>New Password</label><input type=password id=newPassword name=newPassword class=form-control data-ng-model=passwordDetails.newPassword placeholder=\"New Password\"></div><div class=form-group><label for=verifyPassword>Verify Password</label><input type=password id=verifyPassword name=verifyPassword class=form-control data-ng-model=passwordDetails.verifyPassword placeholder=\"Verify Password\"></div><div class=\"text-center form-group\"><button type=submit class=\"btn btn-large btn-primary\">Update Password</button></div><div data-ng-show=error class=\"text-center text-danger\"><strong>{{error}}</strong></div><div data-ng-show=success class=\"text-center text-success\"><strong>{{success}}</strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/users/views/settings/change-password.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/settings/change-password.client.view.html",
    "<section class=row data-ng-controller=SettingsController><h3 class=\"col-md-12 text-center\">Change your password</h3><div class=\"col-xs-offset-2 col-xs-8 col-md-offset-5 col-md-2\"><form data-ng-submit=changeUserPassword() class=\"signin form-horizontal\" autocomplete=off><fieldset><div class=form-group><label for=currentPassword>Current Password</label><input type=password id=currentPassword name=currentPassword class=form-control data-ng-model=passwordDetails.currentPassword placeholder=\"Current Password\"></div><div class=form-group><label for=newPassword>New Password</label><input type=password id=newPassword name=newPassword class=form-control data-ng-model=passwordDetails.newPassword placeholder=\"New Password\"></div><div class=form-group><label for=verifyPassword>Verify Password</label><input type=password id=verifyPassword name=verifyPassword class=form-control data-ng-model=passwordDetails.verifyPassword placeholder=\"Verify Password\"></div><div class=\"text-center form-group\"><button type=submit class=\"btn btn-large btn-primary\">Save Password</button></div><div data-ng-show=success class=\"text-center text-success\"><strong>Password Changed Successfully</strong></div><div data-ng-show=error class=\"text-center text-danger\"><strong data-ng-bind=error></strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/users/views/settings/edit-profile.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/settings/edit-profile.client.view.html",
    "<section class=row data-ng-controller=SettingsController><h3 class=\"col-md-12 text-center\">Edit your profile</h3><div class=\"col-xs-offset-2 col-xs-8 col-md-offset-5 col-md-2\"><form name=userForm data-ng-submit=updateUserProfile(userForm.$valid) class=\"signin form-horizontal\" autocomplete=off><fieldset><div class=form-group><label for=firstName>First Name</label><input id=firstName name=firstName class=form-control data-ng-model=user.firstName placeholder=\"First Name\"></div><div class=form-group><label for=lastName>Last Name</label><input id=lastName name=lastName class=form-control data-ng-model=user.lastName placeholder=\"Last Name\"></div><div class=form-group><label for=email>Email</label><input type=email id=email name=email class=form-control data-ng-model=user.email placeholder=Email></div><div class=form-group><label for=username>Username</label><input id=username name=username class=form-control data-ng-model=user.username placeholder=Username></div><div class=form-group><label for=image>Image</label><input id=image name=image class=form-control data-ng-model=user.image placeholder=Image></div><div class=form-group><label for=username>Permiso</label><input id=username name=username class=form-control data-ng-model=user.roles disabled></div><div class=form-group><label for=image>Description</label><input id=description name=description class=form-control data-ng-model=user.meta.description placeholder=Description></div><div class=form-group><h3>Social Media</h3><label for=meta>Facebook user</label><input id=facebook name=facebook class=form-control data-ng-model=user.meta.facebook placeholder=\"Facebook User\"><label for=image>Twitter user</label><input id=twitter name=twitter class=form-control data-ng-model=user.meta.twitter placeholder=\"Twitter User\"><label for=image>Instagram user</label><input id=instagram name=instagram class=form-control data-ng-model=user.meta.instagram placeholder=\"Instagram User\"><label for=image>Website</label><input id=website name=website class=form-control data-ng-model=user.meta.website placeholder=Website></div><div class=\"text-center form-group\"><button type=submit class=\"btn btn-large btn-primary\">Save Profile</button></div><div data-ng-show=success class=\"text-center text-success\"><strong>Profile Saved Successfully</strong></div><div data-ng-show=error class=\"text-center text-danger\"><strong data-ng-bind=error></strong></div></fieldset></form></div></section>");
}]);

angular.module("modules/users/views/settings/home-settings.client.view.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/settings/home-settings.client.view.html",
    "<section class=row data-ng-controller=SettingsController ng-init=iniciar()><h3 class=\"col-md-12 text-center\">HOME SETTINGS</h3><form name=userForm data-ng-submit=updateUserProfile(userForm.$valid) class=\"signin form-horizontal\" autocomplete=off><div class=row><div class=col-md-7><select ng-model=tipo.type ng-options=\"cat as cat for cat in typeWidget\"></select><div ng-show=\"tipo.type == &quot;category&quot;\"><select ng-model=tipo.category ng-options=\"cat as cat.label for cat in options\" ng-show=\"tipo.type == &quot;category&quot;\"></select><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=tipo.category></select><select ng-model=tipo.count ng-options=\"cat as cat for cat in countOpt\" ng-show=tipo.posicion></select></div><div ng-show=\"tipo.type == &quot;tv&quot;\"><input ng-model=tipo.input ng-show=\"tipo.type == &quot;tv&quot;\"><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=tipo.input></select><select ng-model=tipo.count ng-options=\"cat as cat for cat in countOpt\" ng-show=tipo.posicion></select></div><div ng-show=\"tipo.type == &quot;twitter&quot;\"><select ng-model=tipo.count ng-options=\"cat as cat for cat in countOpt\" ng-show=\"tipo.type == 'twitter'\"></select></div><div ng-if=\"tipo.type == &quot;popular&quot;\"><input ng-model=\"tipo.input = 'popular'\" ng-show=\"tipo.type == 'popular'\" readonly><select ng-model=tipo.count ng-options=\"cat as cat for cat in countOpt\" ng-show=\"tipo.type == 'popular'\"></select></div><div ng-if=\"tipo.type == &quot;likes&quot;\"><input ng-model=\"tipo.input = 'likes'\" ng-show=\"tipo.type == &quot;likes&quot;\" readonly><select ng-model=tipo.count ng-options=\"cat as cat for cat in countOpt\" ng-show=\"tipo.type == 'likes'\"></select></div><div ng-show=\"tipo.type == &quot;hashtag&quot;\"><input ng-model=tipo.input ng-show=\"tipo.type == &quot;hashtag&quot;\"><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=tipo.input></select><select ng-model=tipo.count ng-options=\"cat as cat for cat in countOpt\" ng-show=tipo.posicion></select></div><div ng-show=\"tipo.type == &quot;user&quot;\"><input ng-model=tipo.input ng-show=\"tipo.type == &quot;user&quot;\"><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=tipo.input></select><select ng-model=tipo.count ng-options=\"cat as cat for cat in countOpt\" ng-show=tipo.posicion></select></div><div ng-show=\"tipo.type == &quot;imageday&quot;\"><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=\"tipo.type == 'imageday'\"></select></div><div ng-show=\"tipo.type == &quot;stock&quot;\"><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=\"tipo.type == 'stock'\"></select><input ng-model=tipo.input placeholder=\"Símbolo Gráfico\" ng-show=tipo.posicion><div ng-show=tipo.input><div><input ng-model=symbol placeholder=Símbolos> <i class=\"glyphicon glyphicon-plus\" ng-click=pushArray(symbol)></i></div><ul><li ng-repeat=\"element in tipo.arrayElement track by $index\">{{element}}</li></ul></div></div><div ng-show=\"tipo.type == &quot;custom&quot;\"><input ng-model=tipo.input ng-show=\"tipo.type == &quot;custom&quot;\"><div ng-show=tipo.input><input ng-model=symbol> <i class=\"glyphicon glyphicon-plus\" ng-click=pushArray(symbol)><ul><li ng-repeat=\"element in tipo.arrayElement track by $index\">{{element}}</li></ul></i></div><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=\"tipo.arrayElement.length > 0\"></select></div><div ng-show=\"tipo.type == &quot;top&quot;\"><div ng-show=\"tipo.type == &quot;top&quot;\"><input ng-model=symbol> <i class=\"glyphicon glyphicon-plus\" ng-click=pushArray(symbol)><ul><li ng-repeat=\"element in tipo.arrayElement track by $index\">{{element}}</li></ul></i></div></div><div ng-show=\"tipo.type == &quot;poll&quot;\"><input ng-model=tipo.input ng-show=\"tipo.type == &quot;poll&quot;\"><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=tipo.input></select></div><div ng-show=\"tipo.type == &quot;banner&quot;\"><input ng-model=tipo.input ng-show=\"tipo.type == &quot;banner&quot;\"><select ng-model=tipo.posicion ng-options=\"cat as cat for cat in sideOpt\" ng-show=tipo.input></select></div><button type=button ng-repeat=\"box in boxOpt\" ng-click=addWidget(tipo,box) ng-show=\"\n" +
    "					(tipo.type == 'category' && tipo.count != undefined) ||\n" +
    "					(tipo.type == 'newsletter') ||\n" +
    "					(tipo.type == 'tv' && tipo.count != undefined) || (tipo.type == 'twitter' && tipo.count != undefined)\n" +
    "					|| (tipo.type == 'popular' && tipo.count != undefined) || (tipo.type == 'likes' && tipo.count != undefined)\n" +
    "					|| (tipo.type == 'hashtag' && tipo.count != undefined) || (tipo.type == 'user' && tipo.count != undefined)\n" +
    "					|| (tipo.type == 'imageday' && tipo.posicion != undefined) || (tipo.type == 'stock' && tipo.arrayElement.length > 0)\n" +
    "					|| (tipo.type == 'custom' && tipo.arrayElement.length > 0) || (tipo.type == 'top' && tipo.arrayElement.length > 0)\n" +
    "					|| (tipo.type == 'poll' && tipo.posicion != undefined) || (tipo.type == 'banner' && tipo.posicion != undefined)\n" +
    "					\">{{box}}</button></div><div class=col-md-5><code>{{tipo}}</code></div></div><div class=\"typesDemo row col-m-12\"><div ng-repeat=\"list in lists\" class=col-md-6 ng-class=\"{'clear': $index==2}\"><div class=\"panel panel-info\"><div class=panel-heading><h3 class=panel-title>{{list.label}}</h3></div><div class=panel-body ng-include=\"'modules/users/views/settings/types.html'\"></div></div></div><div class=form-group><input ng-click=sendfile() class=\"btn btn-success\" value=\"Send Config\"></div></div><div class=col-md-12><code>{{lists}}</code></div></form></section>");
}]);

angular.module("modules/users/views/settings/types.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("modules/users/views/settings/types.html",
    "<ul dnd-list=list.widget dnd-allowed-types=list.allowedTypes><li ng-repeat=\"section in list.widget\" dnd-draggable=section dnd-type=section.type dnd-disable-if=\"section.type == 'unknown'\" dnd-moved=\"list.widget.splice($index, 1)\" dnd-effect-allowed=move class=background-{{section.posicion}}><dnd-nodrag><div dnd-handle class=handle>:::</div><div class=name><label type=text class=background-{{section.posicion}}>{{section.type}} - {{section.input}}{{section.category.label}} <span ng-show=\"section.input || section.category.label\">-</span> {{section.posicion}}</label></div><div class=iconset><i class=\"glyphicon glyphicon-triangle-bottom\" style=margin-right:5px></i> <i class=\"glyphicon glyphicon-remove\" data-ng-click=removeItem($index,list.label)></i></div></dnd-nodrag></li><li class=dndPlaceholder>Drop any <strong>{{list.allowedTypes.join(' or ')}}</strong> here</li></ul>");
}]);

angular.module("templates/box-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/box-client-template.html",
    "<article ng-if=\"$ctrl.tipo==1\"><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author>Autor: @<a ng-href=#!/{{::$ctrl.article.user.username}}>{{::$ctrl.article.user.username}}</a></div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><a data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=\"titular-gz no-mg\" ng-if=$ctrl.article.images.url_720 lazy-src={{::$ctrl.article.images.url_720}} animate-visible=true animate-speed=0.5s></a> <a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h2 class=post-title data-ng-bind=$ctrl.article.title></h2></a><div submit-required=true ng-bind-html=\"($ctrl.article.resume | limitTo: 400)\"></div><div class=\"col-xs-12 col-md-12 no-sp\"><span class=\"label label-default tag-gz sp\" ng-repeat=\"tag in $ctrl.article.tags track by $index\"><a data-ng-href=#!/{{::tag.label}}>{{::tag.label}}</a></span></div></div></article><article ng-if=\"$ctrl.tipo==2\"><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author style=top:0>Autor: @<a ng-href=#!/{{::$ctrl.article.user.username}}>{{::$ctrl.article.user.username}}</a></div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><a data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=\"titular-gz no-mg\" ng-if=$ctrl.article.images.thumb_380 lazy-src={{::$ctrl.article.images.thumb_380}} animate-visible=true animate-speed=0.5s></a> <a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h4 class=post-title data-ng-bind=$ctrl.article.title></h4></a></div></article><article ng-if=\"$ctrl.tipo==3\"><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author>Autor: @<a ng-href=#!/{{::$ctrl.article.user.username}}>{{::$ctrl.article.user.username}}</a></div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h1 class=post-title data-ng-bind=$ctrl.article.title></h1></a> <a data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=\"titular-gz no-mg\" ng-if=$ctrl.article.images.url_720 lazy-src={{::$ctrl.article.images.url_720}} animate-visible=true animate-speed=0.5s></a> <a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h2 class=post-title data-ng-bind=$ctrl.article.headlines></h2></a><div ng-bind-html=$ctrl.article.content></div><div class=\"col-xs-12 col-md-12 no-sp\"><span class=\"label label-default tag-gz sp\" ng-repeat=\"tag in $ctrl.article.tags track by $index\"><a data-ng-href=#!/{{::tag.label}}>{{::tag.label}}</a></span></div></div></article><article ng-if=\"$ctrl.tipo==4\"><a data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=titular-gz ng-if=$ctrl.article.images.url_1080 lazy-src={{::$ctrl.article.images.url_1080}} animate-visible=true animate-speed=0.5s></a><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"col-xs-12 col-sm-6 col-md-6 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div></div><div class=\"col-xs-12 col-sm-6 col-md-6\"><span class=tag-gz-tag><span class=\"label label-default tag-gz sp\" ng-repeat=\"tag in $ctrl.article.tags track by $index\"><a data-ng-href=#!/{{::tag.label}}>{{::tag.label}}</a></span></span></div></div><div class=clearfix></div><a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h2 class=post-title>{{::$ctrl.article.title}}</h2><p class=\"col-xs-12 col-md-12 post-content no-sp\"><div submit-required=true ng-bind-html=\"$ctrl.article.resume | limitTo: 400\"></div></p></a></article><article ng-if=\"$ctrl.tipo==5\"><div class=\"col-sm-4 col-md-4 col-xs-4 no-sp\"><div class=dn-sep>“</div></div><div class=\"col-sm-8 col-md-8 col-xs-8 no-sp\"><div class=dn-author>Autor: @<a ng-href=#!/{{::$ctrl.article.user.username}}>{{::$ctrl.article.user.username}}</a></div></div><div class=\"col-sm-12 col-md-12 col-xs-12 no-sp\"><div class=\"dn-head-meta dn-date-share\"><p class=no-mg><a data-ng-href=#!/noticias/category/{{::$ctrl.article.category}}>- {{::option[$ctrl.article.category].label}} -</a> <time class=times am-time-ago=$ctrl.article.published_date></time></p></div><div class=dn-share><a class=sicon-tw href=\"https://twitter.com/share?text={{::$ctrl.article.title}};url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'twitter-share', 'width=550,height=235');return false\"></a> <a class=sicon-f href=\"https://www.facebook.com/sharer/sharer.php?u=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'facebook-share','width=580,height=296');return false\"></a> <a class=sicon-g href=\"https://plus.google.com/share?url=https://www.gazetadecaracas.com/#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}\" onclick=\"window.open(this.href, 'google-plus-share', 'width=490,height=530');return false\"></a></div><a class=post-gz data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><h2 class=post-title data-ng-bind=$ctrl.article.title style=margin-bottom:25px></h2></a><div class=row ng-if=$ctrl.article.images.thumb_380><div class=\"col-md-4 col-xs-12 no-sp\" data-ng-href=#!/articulo/{{::$ctrl.article.slug}}/{{::$ctrl.article.published_date}}><img class=\"titular-gz no-mg\" lazy-src={{::$ctrl.article.images.thumb_380}} animate-visible=true animate-speed=0.5s></div><div class=\"col-md-8 col-xs-12\" submit-required=true ng-bind-html=\"($ctrl.article.resume | limitTo: 300)\"></div></div><div class=row ng-if=!$ctrl.article.images.thumb_380><div class=\"col-md-12 col-xs-12\" submit-required=true ng-bind-html=\"($ctrl.article.resume | limitTo: 300)\"></div></div><div class=\"col-xs-12 col-md-12 no-sp\"><span class=\"label label-default tag-gz sp\" ng-repeat=\"tag in $ctrl.article.tags track by $index\"><a data-ng-href=#!/{{::tag.label}}>{{::tag.label}}</a></span></div></div></article>");
}]);

angular.module("templates/sidebar-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/sidebar-client-template.html",
    "<div class=\"hidden-xs col-sm-5 col-md-4 sidebar\" style=margin-top:50px ng-show=cargado><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp\" style=\"margin-bottom: -100px\"><mostread></mostread><banner banners=banners_B1></banner><hash hash=::etiqueta></hash><chart url=::link></chart><opinion tipo=1></opinion><videos tipo=1></videos><encuesta></encuesta><category idcat=1 tipo=1></category><twitter></twitter><category idcat=2 tipo=1 style=\"margin-bottom:10px;    float: left;\n" +
    "width: 100%\"></category></div><div class=\"col-xs-12 col-sm-12 col-md-12 no-sp sticky\" stickyfill top=100><div><newsletter></newsletter><banner banners=banners_B1></banner><div style=opacity:0.7><img style=width:100% lazy-src=modules/core/img/gazeta-svg/gz-ccs-home.svg><p style=text-align:center>Copyright © 2016 Aviso legal<br>Todos los derechos reservados</p></div></div></div></div>");
}]);

angular.module("templates/tags.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/tags.html",
    "<div class=\"form-control decipher-tags\" data-ng-mousedown=selectArea()><div class=decipher-tags-taglist><span data-ng-repeat=\"tag in tags|orderBy:orderBy\" data-ng-mousedown=$event.stopPropagation()><span class=decipher-tags-tag data-ng-class=getClasses(tag)>{{tag.label}} <i class=\"glyphicon glyphicon-remove-circle\" data-ng-click=removeTag(tag)></i></span></span></div><span class=wrapper data-ng-show=toggles.inputActive><input ng-if=!srcTags.length data-ng-model=inputTag class=\"decipher-tags-input\"> <input ng-if=srcTags.length data-ng-model=inputTag class=decipher-tags-input data-typeahead=\"stag as stag.label for stag in srcTags|filter:$viewValue|orderBy:orderBy\" data-typeahead-input-formatter={{typeaheadOptions.inputFormatter}} data-typeahead-loading={{typeaheadOptions.loading}} data-typeahead-min-length={{typeaheadOptions.minLength}} data-typeahead-template-url={{typeaheadOptions.templateUrl}} data-typeahead-wait-ms={{typeaheadOptions.waitMs}} data-typeahead-delimiter={{typeaheadOptions.delimiter}} data-typeahead-editable={{typeaheadOptions.allowsEditable}} data-typeahead-on-select=\"add($item) && selectArea() && typeaheadOptions.onSelect()\"></span></div>");
}]);

angular.module("templates/uploadFile-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/uploadFile-client-template.html",
    "<div class=row nv-file-drop=\"\" uploader=uploader><h2>Uploads Image</h2><div class=col-md-12 ng-show=\"image_uploaded != undefined\"><div class=col-md-8><img lazy-src={{image_uploaded.thumb_380}} alt=\"\" class=\"img-responsive\"></div><div class=col-md-4><div class=pull-right><button type=button style=margin-bottom:15px class=\"btn btn-danger btn-xs\" ng-click=uploader.queue[0].remove();reset();><span class=\"glyphicon glyphicon-trash\"></span> Remove</button></div></div></div>{{image_uploaded}}<div class=col-md-12 ng-show=\"image_uploaded == undefined\"><h3>Select file</h3><div ng-show=uploader.isHTML5><div class=\"well my-drop-zone\" nv-file-over=\"\" uploader=uploader>Drop File Here</div></div><input type=file nv-file-select=\"\" uploader=\"uploader\"><br></div><div class=col-md-12 style=\"margin-bottom: 40px\"><table class=table><thead><tr><th ng-show=uploader.isHTML5>Size</th><th ng-show=uploader.isHTML5>Progress</th><th>Status</th><th>Actions</th></tr></thead><tbody><tr ng-repeat=\"item in uploader.queue\"><td ng-show=uploader.isHTML5 nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td><td ng-show=uploader.isHTML5><div class=progress style=\"margin-bottom: 0\"><div class=progress-bar role=progressbar ng-style=\"{ 'width': item.progress + '%' }\"></div></div></td></tr></tbody></table></div></div>");
}]);

angular.module("templates/widgets-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/widgets-client-template.html",
    "<div ng-repeat=\"element in widgets | filter: {posicion:$ctrl.pos}\" ng-init=compilar(element)></div>");
}]);

angular.module("templates/youtube-client-template.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("templates/youtube-client-template.html",
    "<div class=\"embed-responsive embed-responsive-16by9\"><iframe id=player class=embed-responsive-item lazy-src={{url}}></iframe></div>");
}]);

angular.module('core').controller('box-controller', ['$scope' ,
	function($scope) {
		$scope.option = [
	    { label: 'Política', value: 0 },
	    { label: 'Economía', value: 1 },
	    { label: 'Sucesos', value: 2 },
	    { label: 'Opinión', value: 3 },
	    { label: 'Imagen del día', value: 4 }
	  	];
	  }]);

'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus', '$http',"$location", '$aside','$rootScope','$document','$timeout',
	function($scope, Authentication, Menus, $http, $location, $aside, $rootScope, $document,$timeout) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.searchView = false;

//aside
		$scope.asideState = {
		      open: false
		};

    $scope.openAside = function(position, backdrop) {
      $scope.asideState = {
        open: true,
        position: position
      };

      function postClose() {
        $scope.asideState.open = false;
      }

      $aside.open({
        templateUrl: 'modules/core/views/aside.html',
        placement: position,
        size: 'xs',
				windowClass: 'menumo',
        backdrop: backdrop,
        controller: function($scope, $uibModalInstance) {
          $scope.ok = function(e) {
            $uibModalInstance.close();
            e.stopPropagation();
          };
          $scope.cancel = function(e) {
            $uibModalInstance.dismiss();
            e.stopPropagation();
          };
        }
      }).result.then(postClose, postClose);
		}
//END aside



		$scope.typeaheadOpts = {
		  minLength: 3,
		  waitMs: 500,
		  allowsEditable: true
		};

		$scope.menu = Menus.getMenu('topbar');

		var clickIn = false;

		$document.bind('click', function() {

			$timeout(function () {
				if(!clickIn){

					$scope.searchView = false;
					clickIn = false;
				}else{

					clickIn = false;
				}
			}, 5);

		});

		$scope.inside = function() {
			clickIn = true;
		};


		$scope.onClick = function(val){

			$scope.searchView = false;

			if(val.id){

				$location.path("/articulo/"+val.id);
				$scope.searchView = !$scope.searchView;
			}
			else {

				$location.path("/busqueda/"+val);
				$scope.searchView = !$scope.searchView;

			}
		}

		$scope.changeSearch = function() {
			$scope.searchView = !$scope.searchView;
		};

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		$scope.tags = ["SIMADI","ISIS","Gazeta", "Test", "Noticia"];

		$scope.queryArray = function(val) {

			var tipos = ["title","headlines"];
			var todo = [];
			var print = {}

			angular.forEach(tipos, function(tipo,key){

					todo.push($http.get('articles?count=6&filter['+ tipo +']='+val, {ignoreLoadingBar: true
									}).then(function(response){
										return response.data.results.map(function(items){
												return items;
									 });
								}));
			});

			print = todo[0].then(function(res){ return res});
			return print&&todo[1];


	};






	}]);

angular.module('core').controller('TagsController', ['$scope', '$http',
	function($scope, $http) {


var url = "tags/"+$stateParams.tags;

$http.get(url,{cache: true}).then(function(data) {
	  $scope.hashNews = data;
});


}]);

angular.module('articles').component('box', {
  bindings: {
    article: "<",
    tipo: "<"
  },
  templateUrl: 'templates/box-client-template.html',
  controller: 'box-controller',
  controllerAs: '$ctrl',
  bindToController: true
});

angular.module('core').directive('autoFocus', ['$timeout' ,
	function($timeout) {

    return {
        restrict: 'AC',
        link: function(_scope, _element) {
            $timeout(function(){
                _element[0].focus();
            }, 0);
        }
    };
}]);

angular.module('core')
.directive('lazySrc', ['$window', '$document', '$rootScope', function($window, $document, $rootScope){
    var doc = $document[0],
        body = doc.body,
        win = $window,
        $win = angular.element(win),
        uid = 0,
        elements = {};

    function getUid(el){
        var __uid = el.data("__uid");
        if (! __uid) {
            el.data("__uid", (__uid = '' + ++uid));
        }
        return __uid;
    }

    function getWindowOffset(){
        var t,
            pageXOffset = (typeof win.pageXOffset == 'number') ? win.pageXOffset : (((t = doc.documentElement) || (t = body.parentNode)) && typeof t.ScrollLeft == 'number' ? t : body).ScrollLeft,
            pageYOffset = (typeof win.pageYOffset == 'number') ? win.pageYOffset : (((t = doc.documentElement) || (t = body.parentNode)) && typeof t.ScrollTop == 'number' ? t : body).ScrollTop;
        return {
            offsetX: pageXOffset,
            offsetY: pageYOffset
        };
    }

    function isVisible(iElement){
        var elem = iElement[0],
            elemRect = elem.getBoundingClientRect(),
            windowOffset = getWindowOffset(),
            winOffsetX = windowOffset.offsetX,
            winOffsetY = windowOffset.offsetY,
            elemWidth = elemRect.width,
            elemHeight = elemRect.height,
            elemOffsetX = elemRect.left + winOffsetX,
            elemOffsetY = elemRect.top + winOffsetY,
            viewWidth = Math.max(doc.documentElement.clientWidth, win.innerWidth || 0),
            viewHeight = Math.max(doc.documentElement.clientHeight, win.innerHeight || 0),
            xVisible,
            yVisible;

        if(elemOffsetY <= winOffsetY){
            if(elemOffsetY + elemHeight >= winOffsetY){
                yVisible = true;
            }
        }else if(elemOffsetY >= winOffsetY){
            if(elemOffsetY <= winOffsetY + viewHeight){
                yVisible = true;
            }
        }

        if(elemOffsetX <= winOffsetX){
            if(elemOffsetX + elemWidth >= winOffsetX){
                xVisible = true;
            }
        }else if(elemOffsetX >= winOffsetX){
            if(elemOffsetX <= winOffsetX + viewWidth){
                xVisible = true;
            }
        }

        return xVisible && yVisible;
    };

    function checkImage(){
        Object.keys(elements).forEach(function(key){
            var obj = elements[key],
                iElement = obj.iElement,
                $scope = obj.$scope;
            if(isVisible(iElement)){
                iElement.attr('src', $scope.lazySrc);
            }
        });
    }

    $win.bind('scroll', checkImage);
    $win.bind('resize', checkImage);
    checkImage();
    $rootScope.$on('tv',checkImage);

    function onLoad(){
        var $el = angular.element(this),
            uid = getUid($el);

        $el.css('opacity', 1);

        if(elements.hasOwnProperty(uid)){
            delete elements[uid];
        }
    }

    return {
        restrict: 'A',
        scope: {
            lazySrc: '@',
            animateVisible: '@',
            animateSpeed: '@'
        },
        link: function($scope, iElement){

            iElement.bind('load', onLoad);

            $scope.$watch('lazySrc', function(){
                var speed = "1s";
                if ($scope.animateSpeed != null) {
                    speed = $scope.animateSpeed;
                }
                if(isVisible(iElement)){
                    if ($scope.animateVisible) {
                        iElement.css({
                           //'background-color': '#d5d5d5',
                            'opacity': 0,
                            '-webkit-transition': 'opacity ' + speed,
                            'transition': 'opacity ' + speed
                        });
                    }
                    iElement.attr('src', $scope.lazySrc);
                }else{
                    var uid = getUid(iElement);
                    iElement.css({
                        //'background-color': '#d5d5d5',
                        'opacity': 0,
                        '-webkit-transition': 'opacity ' + speed,
                        'transition': 'opacity ' + speed
                    });
                    elements[uid] = {
                        iElement: iElement,
                        $scope: $scope
                    };
                }
            });

            $scope.$on('$destroy', function(){
                iElement.unbind('load');
                var uid = getUid(iElement);
                if(elements.hasOwnProperty(uid)){
                    delete elements[uid];
                }
            });
        }
    };
}]);

'use strict';
angular.module('core').component('sidebar', {
  templateUrl: "templates/sidebar-client-template.html"
});

angular.module('articles').directive('uiScrollfix', ['$window', function ($window) {
  'use strict';

  function getWindowScrollTop() {
    if (angular.isDefined($window.pageYOffset)) {
      return $window.pageYOffset;
    } else {
      var iebody = (document.compatMode && document.compatMode !== 'BackCompat') ? document.documentElement : document.body;
      return iebody.scrollTop;
    }
  }
  return {
    require: '^?uiScrollfixTarget',
    link: function (scope, elm, attrs, uiScrollfixTarget) {
      var absolute = true,
          shift = 0,
          fixLimit,
          $target = uiScrollfixTarget && uiScrollfixTarget.$element || angular.element($window);

      if (!attrs.uiScrollfix) {
          absolute = false;
      } else if (typeof(attrs.uiScrollfix) === 'string') {
        // charAt is generally faster than indexOf: http://jsperf.com/indexof-vs-charat
        if (attrs.uiScrollfix.charAt(0) === '-') {
          absolute = false;
          shift = - parseFloat(attrs.uiScrollfix.substr(1));
        } else if (attrs.uiScrollfix.charAt(0) === '+') {
          absolute = false;
          shift = parseFloat(attrs.uiScrollfix.substr(1));
        }
      }

      fixLimit = absolute ? attrs.uiScrollfix : elm[0].offsetTop + shift;

      function onScroll() {

        var limit = absolute ? attrs.uiScrollfix : elm[0].offsetTop + shift;

        // if pageYOffset is defined use it, otherwise use other crap for IE
        var offset = uiScrollfixTarget ? $target[0].scrollTop : getWindowScrollTop();
        if (!elm.hasClass('ui-scrollfix') && offset > limit) {
          elm.addClass('ui-scrollfix');
          fixLimit = limit;
        } else if (elm.hasClass('ui-scrollfix') && offset < fixLimit) {
          elm.removeClass('ui-scrollfix');
        }
      }

      $target.on('scroll', onScroll);

      // Unbind scroll event handler when directive is removed
      scope.$on('$destroy', function() {
        $target.off('scroll', onScroll);
      });
    }
  };
}]).directive('uiScrollfixTarget', [function () {
  'use strict';
  return {
    controller: ['$element', function($element) {
      this.$element = $element;
    }]
  };
}]);
angular.module('articles').directive('youtube', ['$window', 'youtubeApiLoader', '$animate' ,function($window, youtubeApiLoader, $animate) {
  return {
    restrict: "E",

    scope: {
      stateChange: "&",
      playerReady: "&",
      playbackQualityChange: "&",
      playbackRateChange: "&",
      error: "&",
      apiChange: "&",
      videoId: "<", // required
      width: "@",
      height: "@",
      autoHide: "@",
      autoPlay: "@",
      ccLoadPolicy: "@",
      color: "@",
      control: "@",
      disableKb: "@",
      enableJsApi: "@",
      end: "@",
      fs: "@",
      hl: "@",
      ivLoadPolicy: "@",
      list: "@",
      listType: "@",
      loop: "@",
      modestBranding: "@",
      origin: "@",
      playerApiId: "@",
      playlist: "@",
      playsInline: "@",
      rel: "@",
      showInfo: "@",
      start: "@",
      theme: "@",
      mute: "="
    },

    template: '<div></div>',

    link: function(scope, element, attrs) {

    console.log(scope.videoId);

    $animate.addClass(element[0],'embed-responsive embed-responsive-16by9');
    $animate.addClass(element.children()[0],'embed-responsive-item');


      if(!scope.width && !scope.height){
        scope.width = 640;
        scope.height = 480;
      }

      scope.$watch('mute',function() {
        if(scope.mute != undefined){
          if(scope.mute && scope.player != undefined){
             console.log('mute');
              scope.player.mute();
          }else if(!scope.mute && scope.player != undefined){
              console.log('unmute');
              scope.player.unMute();
          }
        }
      });


      youtubeApiLoader.ready.then(function() {
        scope.player = new YT.Player(element.children()[0], {
          playerVars: {
            autohide: scope.autoHide,
            autoplay: scope.autoPlay,
            cc_load_policy: scope.ccLoadPolicy,
            color: scope.color,
            controls: scope.control,
            disablekb: scope.disableKb,
            enablejsapi: scope.enableJsApi,
            end: scope.end,
            fs: scope.fs,
            hl: scope.hl,
            iv_load_policy: scope.ivLoadPolicy,
            list: scope.list,
            listType: scope.listType,
            loop: scope.loop,
            modestbranding: scope.modestBranding,
            origin: scope.origin,
            playerapiid: scope.playerApiId,
            playlist: scope.playlist,
            playsinline: scope.playsInline,
            rel: scope.rel,
            showinfo: scope.showInfo,
            start: scope.start,
            theme: scope.theme
          },

          height: scope.height,
          width: scope.width,
          videoId: scope.videoId,
          events: {
            'onReady': function(event) {
              console.log(scope.player);
              if(scope.mute){
                scope.player.mute();
              }
              scope.playerReady({
                event: event
              });
            },
            'onStateChange': function(event) {
              scope.stateChange({
                event: event
              });
            },
            'onPlaybackQualityChange': function(event) {
              scope.playbackQualityChange({
                event: event
              });
            },
            'onPlaybackRateChange': function(event) {
              scope.playbackRateChange({
                event: event
              });
            },
            'onError': function(event) {
              scope.error({
                event: event
              });
            },
            'onApiChange': function(event) {
              scope.apiChange({
                event: event
              });
            }
          }
        });
      });



    }
  };
}]);

angular.module('core').service('anchorSmoothScroll', [ function() {


    this.scrollTo = function(eID) {

        // This scrolling function 
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
        
        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for ( var i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }

    };
    
}]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('core').factory('Articles', ['$resource',
	function($resource) {
		return $resource('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/articles/:articleId', {
			articleId: '@slug'
		}, {
			update: {
				method: 'PUT'
			},
			save: {
				url:'articles/',
				method: 'POST'
			}
		});
	}
]);

'use strict';

//Banners service used to communicate Banners REST endpoints
angular.module('core').factory('Banners', ['$resource',
	function($resource) {
		return $resource('banners/:bannerId',
		{ bannerId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			$update: {
				method: 'PUT'
			},
			save: {
				url:'banners/',
				method: 'POST'
			}
		});
	}
]);

angular.module('articles').factory('Imagenes', ["$http", "Articles", function($http, Articles) {
  var Imagenes = function() {

      var foto = "";
      var url = "/image";
      $http.get(url, {cache: true}).then(function(data){foto = data;});



  };


return Imagenes.foto;

}]);

'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
	}
]);
'use strict';

//Polls service used to communicate Polls REST endpoints
angular.module('core').factory('Polls', ['$resource',
	function($resource) {
		return $resource('polls/:pollId', { pollId: '@_id'
		}, {
			update: {
				url:'polls/:pollId',
				method: 'PUT'
			}
		},
		{
			update: {
				url:'polls/:pollId',
				method: 'POST'
			}
		},
		{
 			query: {
 			method: 'GET',
 			params: { pollId: 'polls' },
			cache: true,
 			ignoreLoadingBar: true
 		}
 	});
	}
]);

angular.module('core').factory('Vote', ['$resource',
	function($resource) {
		return $resource('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/vote/:pollId', { pollId: '@_id'
		} ,
		{
 			save: {
 			method: 'POST',
 			params: { pollId: 'polls'},
 			ignoreLoadingBar: true
 		}
 	});
	}
]);

'use strict';

//Tags service used to communicate Tags REST endpoints
angular.module('core').factory('Tags', ['$resource',
	function($resource) {
		return $resource('tags/:tagId', { tagId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('core').factory('Etiquetas', ["$http", "$location", "Tags", function($http, $location, Tags) {

  var Etiquetas = function() {
    var states = [];

    Tags.query({}, function(response) {

      var todo = response;

          angular.forEach(todo , function(element, i){
              states.push(element.label);
           });

      });

     return states;
  }

  return Etiquetas;



}]);

"use strict";

angular.module('core').factory('youtubeApiLoader', ['$q', '$window', function($q, $window) {
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  var loaded = false;
  var defer = $q.defer();

  $window.onYouTubeIframeAPIReady = function() {
    defer.resolve();
  };

  return {
    ready: defer.promise
  };
}]);

'use strict';

// Configuring the Articles module
angular.module('polls').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Encuestas', 'polls/list', '/polls(/create)?');
	}
]);

'use strict';

//Setting up route
angular.module('polls').config(['$stateProvider',
	function($stateProvider) {
		// Polls state routing
		$stateProvider.
		state('listPolls', {
			url: '/polls/list',
			templateUrl: 'modules/polls/views/list-polls.client.view.html'
		}).
		state('createPoll', {
			url: '/polls/create',
			templateUrl: 'modules/polls/views/create-poll.client.view.html'
		}).
		state('viewPoll', {
			url: '/polls/:pollId',
			templateUrl: 'modules/polls/views/view-poll.client.view.html'
		}).
		state('editPoll', {
			url: '/polls/:pollId/edit',
			templateUrl: 'modules/polls/views/edit-poll.client.view.html'
		});
	}
]);

'use strict';

// Polls controller
angular.module('polls').controller('PollsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Polls',
	function($scope, $stateParams, $location, Authentication, Polls) {
		$scope.authentication = Authentication;

		// Create new Poll
		$scope.create = function() {
			// Create new Poll object
			var poll = new Polls ({
				question: this.question,
				options: $scope.options
			});

			// Redirect after save
			poll.$save(function(response) {
				$location.path('polls/' + response._id);

				// Clear form fields
				$scope.question = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Poll
		$scope.remove = function(poll) {

			if (poll) {
				Polls.delete({
					pollId: poll._id
				},function() {
					$location.path('polls/list');
				});

				for (var i in $scope.polls) {
					if ($scope.polls[i] === poll) {
						$scope.polls.splice(i, 1);
					}
				}
			} else {
				$scope.poll.$remove({
					pollId: $stateParams.pollId
				}, function() {
					$location.path('polls/list');
				});
			}

		};

		// Update existing Poll
		$scope.update = function() {
			var poll = $scope.poll;

			poll.options = $scope.options;

			poll.$update(function() {
				$location.path('polls/' + poll._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Polls
		$scope.find = function() {
			Polls.get(function(response){
				console.log(response);
				$scope.polls = response.Poll;
			});
		};


		// Find existing Poll
		$scope.findOne = function() {
			$scope.options = [];
			$scope.poll = Polls.get({
				pollId: $stateParams.pollId
			}, function(data){

				$scope.options = data.options;

			});

		};

		var indice = undefined;
		$scope.inicializar = function(){
			$scope.options =[];
			indice = 0;
		};



		$scope.addOpt = function(){

			var dato = {text : $scope.dataOpt, value: indice, vote:0};
			$scope.options.push(dato);
			console.log($scope.options);
			$scope.dataOpt = "";
			indice++;
		};

		$scope.removeOpt = function(ind){
			$scope.options.splice(ind,1);
		}

	}
]);

'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);
'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('homeSettings', {
			url: '/settings/home',
			templateUrl: 'modules/users/views/settings/home-settings.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('signup', {
			url: '/user/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html',
			controller:'AuthenticationController'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token/:user',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);

'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		if(!$scope.authentication.user){
			$location.path('/');
		}

		$scope.signin = function() {
			if ($scope.authentication.user) $location.path('/');
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// If user is signed in then redirect back home


		$scope.signup = function() {
			if (!$scope.authentication.user) $location.path('/');
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};


	}
]);

'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
	function($scope, $stateParams, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/reset/' + $stateParams.token + '/'+ $stateParams.user, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);

'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication',
	function($scope, $http, $location, Users, Authentication) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

				$scope.sendfile = function(){
					$http.put('/settingsfile', $scope.lists).then( function(err, data){
							if(err){console.log(err);}else{
								$http.get('/config.json').then(function(response, err){
										if(err){console.log(err);}else{
											console.log(response.data);
										 $scope.lists =	angular.fromJson(response.data);
										}
								});
							}
					});
				};

				$scope.iniciar = function(){

					$http.get('/config.json').then(function(response, err){
							if(err){console.log(err);}else{
							 $scope.lists =	angular.fromJson(response.data);
							}
					});
				}

				$scope.tipo = {
					arrayElement:[]
				};

				//Widget Types
				$scope.typeWidget = [
					'category', 'tv', 'newsletter', 'twitter', 'stock', 'popular', 'likes' , 'hashtag', 'user' , 'custom', 'imageday', 'top', 'poll', 'banner'
				]

				//Count Options
				$scope.countOpt = [
				   1,3,5,7,9,11,13
				]

				//Side Options
				$scope.sideOpt = [
				   'Principal','Lateral'
				]

				//Section Type
				$scope.options = [
					{ label: 'Política', value: 0 },
					{ label: 'Economía', value: 1 },
					{ label: 'Sucesos', value: 2 },
					{ label: 'Opinión', value: 3 }
					];

					//Label Options
					$scope.boxOpt = [
					'Home',
					'Política',
					'Economía',
					'Sucesos',
					'Opinión'
						];

				//Push to array function
				$scope.pushArray = function(element){
					$scope.tipo.arrayElement.push(element);
					$scope.symbol = '';
				}

				//Add widget function
				$scope.addWidget = function(tipo, box){
						if(tipo.type == 'newsletter' || tipo.type == 'twitter' || tipo.type == 'popular' || tipo.type =='likes'){
							tipo.posicion = 'Lateral';
						}
						angular.forEach($scope.lists, function(element){
							if(element.label == box){
								element.widget.push(tipo);
							}
						});
						$scope.tipo = {
							arrayElement:[]
						};
				}

				$scope.removeItem = function(index, box){
					angular.forEach($scope.lists, function(element){
						if(element.label == box){
							element.widget.splice(index, 1);
						}
					});

				 }

				//List of labels
				$scope.lists = [
				{
						label: "Home",
						widget: []
				},
				{
						label: "Política",
						widget: []
				},
				{
						label: "Economía",
						widget: []
				},
				{
						label: "Sucesos",
						widget: []
				},
				{
						label: "Opinión",
						widget: []
				}
		];

		// Model to JSON for demo purpose
		$scope.$watch('lists', function(lists) {
				$scope.modelAsJson = angular.toJson(lists, true);
		}, true);


		//Section Options
		$scope.options = [
	    { label: 'Política', value: 0 , side: false},
	    { label: 'Economía', value: 1, side: false },
	    { label: 'Sucesos', value: 2, side: false },
	    { label: 'Opinión', value: 3, side: false }
	  	];


		// Check if there are additional accounts
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [
	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);
'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);